package com.shaza.videofeature.loginmodule.model

/**
 * Created by Shaza Hassan on 22-Nov-2021
 */
data class LoginResponse(
    val userUid: String
)