package com.shaza.videofeature.loginmodule.model.repo

import com.shaza.videofeature.helper.firebaseauthenticationhelper.FirebaseAuthenticationHelper
import com.shaza.videofeature.loginmodule.model.LoginResponse
import io.reactivex.subjects.PublishSubject

/**
 * Created by Shaza Hassan on 22-Nov-2021
 */
class LoginRepo {

    fun loginWithEmailAndPassword(email: String, password: String): PublishSubject<LoginResponse> {
        val publishSubject: PublishSubject<LoginResponse> = PublishSubject.create()
        FirebaseAuthenticationHelper.loginWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val currentUser = FirebaseAuthenticationHelper.currentUser()
                    val loginResponse = currentUser?.uid?.let { LoginResponse(it) }
                    loginResponse?.let { publishSubject.onNext(it) }
                    publishSubject.onComplete()
                } else {
                    task.exception?.let { publishSubject.onError(it) }
                    publishSubject.onComplete()
                }
            }

        return publishSubject
    }

}