package com.shaza.videofeature.loginmodule.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.R
import com.shaza.videofeature.VideoApplication
import com.shaza.videofeature.helper.internetconnectionhelper.InternetConnectionHelper
import com.shaza.videofeature.loginmodule.model.repo.LoginRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    application: Application,
    private val repo: LoginRepo
) : AndroidViewModel(application) {
    private val tag = this::class.java.simpleName

    private val emailLiveData = MutableLiveData<String>()
    private val passwordLiveData = MutableLiveData<String>()

    var errorInForm: Boolean = false

    var errMsg = MutableLiveData<String>()
    var showLoadingIndicator = MutableLiveData<Boolean>()

    var currentUser = MutableLiveData<String>()

    val compositeDisposable = CompositeDisposable()

    fun updateEmail(email: String) {
        emailLiveData.value = email
    }

    fun updatePassword(password: String) {
        passwordLiveData.value = password
    }

    fun emailIsEmptyOrNull(): Boolean {
        return emailLiveData.value?.trim()?.isEmpty() ?: true
    }

    fun passwordIsEmptyOrNull(): Boolean {
        return passwordLiveData.value?.trim()?.isEmpty() ?: true
    }

    fun login() {
        if (isConnected()) {
            if (!errorInForm) {
                loginWithEmailAndPassword()
            }
        } else {
            errMsg.value =
                getApplication<VideoApplication>().getString(R.string.internet_connection_error)
        }
    }

    private fun loginWithEmailAndPassword() {
        val observable =
            repo.loginWithEmailAndPassword(emailLiveData.value!!, passwordLiveData.value!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    showLoadingIndicator.postValue(true)
                }
                .doOnNext {
                    currentUser.postValue(it.userUid)
                }
                .doOnError {
                    errMsg.postValue(it.message)
                    showLoadingIndicator.postValue(false)
                }.doOnComplete {
                    showLoadingIndicator.postValue(false)
                }

        compositeDisposable.addAll(
            observable.subscribe(
                { Log.v(tag, "Login successfully") },
                { Log.e(tag, "Login failed") }
            )
        )
    }

    private fun isConnected(): Boolean {
        return InternetConnectionHelper.isConnected(getApplication())
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}