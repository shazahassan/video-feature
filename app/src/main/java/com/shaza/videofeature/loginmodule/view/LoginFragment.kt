package com.shaza.videofeature.loginmodule.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.shaza.videofeature.MainActivity
import com.shaza.videofeature.R
import com.shaza.videofeature.homemodule.view.HomeFragment
import com.shaza.videofeature.loginmodule.viewmodel.LoginViewModel
import com.shaza.videofeature.sharedmodule.ErrorPopup
import com.shaza.videofeature.signupmodule.view.SignupFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.login_fragment.*

@AndroidEntryPoint
class LoginFragment : Fragment() {
    val tagLogin: String = this::class.java.simpleName

    companion object {
        fun newInstance() = LoginFragment()
    }

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        initListener()
    }

    private fun initObserver() {
        viewModel.showLoadingIndicator.observe(viewLifecycleOwner, {
            handleLoadingIndicator(it)
        })

        viewModel.errMsg.observe(viewLifecycleOwner, {
            handleShowError(it)
        })

        viewModel.currentUser.observe(viewLifecycleOwner, {
            (requireActivity() as MainActivity).removePreviousFragment()
            (requireActivity() as MainActivity).goToFragment(HomeFragment.newInstance(it))
        })
    }

    private fun handleLoadingIndicator(it: Boolean) {
        if (it) {
            progress.visibility = VISIBLE
        } else {
            progress.visibility = GONE
        }
    }

    private fun handleShowError(errorMsg: String) {
        ErrorPopup.show(requireContext(), errorMsg)
    }

    private fun initListener() {

        loginButton.setOnClickListener {
            it.isEnabled = false
            checkFields()
            viewModel.login()
            it.isEnabled = true
        }

        dontHaveAccount.setOnClickListener {
            (requireActivity() as MainActivity).goToFragment(SignupFragment.newInstance(), true)
        }

        emailEditText.doOnTextChanged { text, _, _, _ ->
            hideErrorField(emailEditText)
            viewModel.updateEmail(text.toString())
        }

        passwordEditText.doOnTextChanged { text, _, _, _ ->
            hideErrorField(passwordEditText)
            viewModel.updatePassword(text.toString())
        }
    }

    private fun hideErrorField(textInputLayout: EditText) {
        viewModel.errorInForm = false
        textInputLayout.error = null
    }

    private fun showErrorInField(textInputLayout: EditText, errorMsg: String) {
        viewModel.errorInForm = true
        textInputLayout.error = errorMsg
    }

    private fun checkFields() {
        if (viewModel.emailIsEmptyOrNull()) {
            showErrorInField(emailEditText, getString(R.string.email_error))
        }

        if (viewModel.passwordIsEmptyOrNull()) {
            showErrorInField(passwordEditText, getString(R.string.password_error))
        }
    }
}