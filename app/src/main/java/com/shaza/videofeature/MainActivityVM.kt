package com.shaza.videofeature

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.helper.firebaseauthenticationhelper.FirebaseAuthenticationHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */
@HiltViewModel
class MainActivityVM @Inject constructor(application: Application) : AndroidViewModel(application) {

    val isLoggedInUser = MutableLiveData<Boolean>()
    var userUid: String? = null

    fun checkIfThereIsLoggedInUser() {
        val currentUser = FirebaseAuthenticationHelper.currentUser()
        if (currentUser == null) {
            isLoggedInUser.value = false
        } else {
            isLoggedInUser.value = true
            userUid = currentUser.uid
        }
    }
}