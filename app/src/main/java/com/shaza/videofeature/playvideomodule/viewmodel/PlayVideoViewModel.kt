package com.shaza.videofeature.playvideomodule.viewmodel

import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.playvideomodule.model.repo.PlayVideoRepo
import com.shaza.videofeature.playvideomodule.view.PlayVideoActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


@HiltViewModel
class PlayVideoViewModel @Inject constructor(
    application: Application,
    private val repo: PlayVideoRepo
) : AndroidViewModel(application) {

    private val compositeDisposable = CompositeDisposable()


    val errMsg = MutableLiveData<String>()
    val infoMsg = MutableLiveData<String>()
    val showLoadingIndicator = MutableLiveData<Boolean>()

    private val videoName = MutableLiveData<String>()
    private val userUid = MutableLiveData<String>()
    val downloadUrl = MutableLiveData<String>()

    fun extractData(arguments: Bundle?) {
        videoName.value = arguments?.getString(PlayVideoActivity.videoNameKey)
        userUid.value = arguments?.getString(PlayVideoActivity.userUidKey)
        getVideoDownloadUrl()
    }

    fun getVideoDownloadUrl() {
        if (videoName.value != null && userUid.value != null) {
            val observable = repo.getVideoUrl(videoName.value!!, userUid.value!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
//                    showLoadingIndicator.postValue(true)
                }
                .doOnNext {
                    downloadUrl.postValue(
                        it
                    )
                }
                .doOnError {
                    errMsg.postValue(it.message)
                }
                .doOnComplete {
//                    showLoadingIndicator.postValue(false)
                }

            compositeDisposable.add(
                observable.subscribe(
                    { Log.v("tag", "") },
                    { Log.e("tag", it.message.toString()) }
                )
            )
        }
    }

    fun seenThisVideo() {
        if (videoName.value != null && userUid.value != null) {

            val observable = repo.seenThisVideo(videoName.value!!, userUid.value!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                }
                .doOnError {
                    errMsg.postValue(it.message)
                }
                .doOnComplete {
                    infoMsg.postValue("Marked as seen")
                }

            compositeDisposable.add(
                observable.subscribe(
                    { Log.v("tag", "") },
                    { Log.e("tag", it.message.toString()) }
                )
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}