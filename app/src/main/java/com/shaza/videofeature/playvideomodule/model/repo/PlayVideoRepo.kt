package com.shaza.videofeature.playvideomodule.model.repo

import com.shaza.videofeature.helper.firebasefirestorehelper.FirebaseFirestoreHelper
import com.shaza.videofeature.helper.firebasestoragehelper.FirebaseStorageHelper
import io.reactivex.subjects.PublishSubject

/**
 * Created by Shaza Hassan on 27-Nov-2021
 */
class PlayVideoRepo {

    fun getVideoUrl(videoName: String, userUid: String): PublishSubject<String> {
        val publishSubject: PublishSubject<String> = PublishSubject.create()
        FirebaseStorageHelper.getDownloadUrl(videoName, userUid).downloadUrl.addOnCompleteListener {
            if (it.isSuccessful) {
                publishSubject.onNext(
                    it.result.toString()
                )
                publishSubject.onComplete()
            } else {
                it.exception?.let { exception -> publishSubject.onError(exception) }
            }
        }
        return publishSubject
    }

    fun seenThisVideo(videoName: String, userUid: String): PublishSubject<Any> {
        val publishSubject: PublishSubject<Any> = PublishSubject.create()
        FirebaseFirestoreHelper.updateSeenStatus(videoName, userUid)
            .addOnCompleteListener { publishSubject.onComplete() }
            .addOnFailureListener { publishSubject.onError(it) }
        return publishSubject
    }
}