package com.shaza.videofeature.playvideomodule.view

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo.*
import android.os.Bundle
import android.view.View
import android.widget.MediaController
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.shaza.videofeature.R
import com.shaza.videofeature.playvideomodule.viewmodel.PlayVideoViewModel
import com.shaza.videofeature.receiver.OnConnected
import com.shaza.videofeature.sharedmodule.ErrorPopup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.play_video_activity.*


@AndroidEntryPoint
class PlayVideoActivity : AppCompatActivity(), OnConnected {

    val TAG = this::class.java.simpleName
    lateinit var mediaController: MediaController

    companion object {
        const val userUidKey = "USER_ID"
        const val videoNameKey = "VIDEO_NAME"
        fun newInstance(activity: Activity, userUid: String, videoName: String): Intent {
            val playVideoFragment = Intent(activity, PlayVideoActivity::class.java)
            val bundle = Bundle()
            bundle.putString(userUidKey, userUid)
            bundle.putString(videoNameKey, videoName)
            playVideoFragment.putExtras(bundle)
            return playVideoFragment
        }
    }

    private val viewModel: PlayVideoViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.play_video_activity)
        mediaController = MediaController(this)
        viewModel.extractData(this.intent.extras)
        initListener()
        initObserver()
    }

    override fun onStart() {
        super.onStart()
        handleLoadingIndicator(true)
    }

    private fun initListener() {
        videoView.setOnPreparedListener {
            mediaController.show(0)
            handleLoadingIndicator(false)
            viewModel.seenThisVideo()
            requestedOrientation = if (videoView.width < videoView.height)
                SCREEN_ORIENTATION_PORTRAIT else
                SCREEN_ORIENTATION_LANDSCAPE
            videoView.start()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        requestedOrientation = SCREEN_ORIENTATION_UNSPECIFIED
    }

    private fun initObserver() {
        viewModel.downloadUrl.observe(this, {
            videoView.setVideoPath(it)
            mediaController.requestFocus()
            mediaController.setAnchorView(videoView)
            videoView.setMediaController(mediaController)
        })

        viewModel.showLoadingIndicator.observe(this, {
            handleLoadingIndicator(it)
        })

        viewModel.errMsg.observe(this, {
            handleErrMsg(it)
        })

        viewModel.infoMsg.observe(this, {
//            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    override fun requestData() {
        if (!videoView.isPlaying) {
            viewModel.getVideoDownloadUrl()
        }
    }

    private fun handleLoadingIndicator(showLoading: Boolean) {
        if (showLoading) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }

    }

    private fun handleErrMsg(errorMsg: String) {
        errorPopup(errorMsg)
    }

    private fun errorPopup(errMsg: String) {
        ErrorPopup.show(this, errMsg)
    }
}