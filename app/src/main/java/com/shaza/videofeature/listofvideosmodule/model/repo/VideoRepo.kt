package com.shaza.videofeature.listofvideosmodule.model.repo

import com.google.firebase.firestore.ktx.toObject
import com.shaza.videofeature.helper.firebasefirestorehelper.FirebaseFirestoreHelper
import com.shaza.videofeature.model.SentVideoModel
import io.reactivex.subjects.PublishSubject

/**
 * Created by Shaza Hassan on 24-Nov-2021
 */

class VideoRepo {

    fun getListVideos(workerId: String): PublishSubject<MutableList<SentVideoModel>> {
        val publishSubject: PublishSubject<MutableList<SentVideoModel>> = PublishSubject.create()

        FirebaseFirestoreHelper.getAllVideosForHealthWorker(workerId)
            .addSnapshotListener { value, error ->
                if (error != null) {
                    publishSubject.onError(error)
                }

                if (value?.documents.isNullOrEmpty()) {
                    publishSubject.onNext(mutableListOf())
                } else {
                    val list = mutableListOf<SentVideoModel>()
                    for (document in value!!.documents) {
                        val documentObject = document.toObject<SentVideoModel>()
                        documentObject?.let { list.add(it) }
                    }
                    publishSubject.onNext(list)
                }
            }

        return publishSubject
    }

    fun listenToWatchedVideos(workerId: String): PublishSubject<MutableList<SentVideoModel>> {
        val publishSubject: PublishSubject<MutableList<SentVideoModel>> = PublishSubject.create()

        FirebaseFirestoreHelper.listenToWatchedVideo(workerId).addSnapshotListener { value, error ->
            if (error != null) {
                publishSubject.onError(error)
            }

            if (value?.documents.isNullOrEmpty()) {
                publishSubject.onNext(mutableListOf())
            } else {
                val list = mutableListOf<SentVideoModel>()
                for (document in value!!.documents) {
                    val documentObject = document.toObject<SentVideoModel>()
                    documentObject?.let { list.add(it) }
                }
                publishSubject.onNext(list)
            }
        }
        return publishSubject
    }

    fun listenToNewVideos(workerId: String): PublishSubject<MutableList<SentVideoModel>> {
        val publishSubject: PublishSubject<MutableList<SentVideoModel>> = PublishSubject.create()

        FirebaseFirestoreHelper.listenToNewVideos(workerId).addSnapshotListener { value, error ->
            if (error != null) {
                publishSubject.onError(error)
            }

            if (value?.documents.isNullOrEmpty()) {
                publishSubject.onNext(mutableListOf())
            } else {
                val list = mutableListOf<SentVideoModel>()
                for (document in value!!.documents) {
                    val documentObject = document.toObject<SentVideoModel>()
                    documentObject?.let { list.add(it) }
                }
                publishSubject.onNext(list)
            }
        }
        return publishSubject
    }
}