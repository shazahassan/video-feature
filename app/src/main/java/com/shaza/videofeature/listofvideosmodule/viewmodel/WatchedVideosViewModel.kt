package com.shaza.videofeature.listofvideosmodule.viewmodel

import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.listofvideosmodule.model.repo.VideoRepo
import com.shaza.videofeature.listofvideosmodule.view.ListOfVideosFragment
import com.shaza.videofeature.model.SentVideoModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class WatchedVideosViewModel @Inject constructor(
    application: Application,
    private val repo: VideoRepo
) : AndroidViewModel(application) {

    private val tag = this::class.java.simpleName

    val videos = MutableLiveData<MutableList<SentVideoModel>>()
    val workerId = MutableLiveData<String>()


    val showLoadingIndicator = MutableLiveData<Boolean>()

    val errMsg = MutableLiveData<String>()

    private val compositeDisposable = CompositeDisposable()

    fun extractData(argument: Bundle?) {
        workerId.value = argument?.getString(ListOfVideosFragment.workerIdKey)
    }

    fun listenToVideos() {
        val workerId = workerId.value
        if (workerId != null) {
            val observable = repo.listenToWatchedVideos(workerId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    showLoadingIndicator.postValue(true)
                }
                .doOnNext {
                    videos.postValue(it)
                    showLoadingIndicator.postValue(false)
                }
                .doOnError {
                    errMsg.postValue(it.message)
                    showLoadingIndicator.postValue(false)
                }

            compositeDisposable.add(
                observable.subscribe(
                    { Log.v(tag, "data got") },
                    { Log.e(tag, it.message.toString()) }
                )
            )

        }
    }
}