package com.shaza.videofeature.listofvideosmodule.view.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.shaza.videofeature.R
import com.shaza.videofeature.helper.firebasefirestorehelper.FirebaseFirestoreHelper
import com.shaza.videofeature.helper.firebasestoragehelper.FirebaseStorageHelper
import com.shaza.videofeature.helper.glidehelper.GlideApp
import com.shaza.videofeature.model.SentVideoModel
import kotlinx.android.synthetic.main.upload_video_item.view.*
import kotlinx.android.synthetic.main.video_item.view.*
import kotlinx.android.synthetic.main.video_item.view.duration
import kotlinx.android.synthetic.main.video_item.view.sentTime
import kotlinx.android.synthetic.main.video_item.view.seperator
import kotlinx.android.synthetic.main.video_item.view.videoName
import kotlinx.android.synthetic.main.video_item.view.videoStatus
import kotlinx.android.synthetic.main.video_item.view.videoThumbnail
import java.io.File

/**
 * Created by Shaza Hassan on 24-Nov-2021
 */

class VideosAdapter(
    var videos: MutableList<SentVideoModel>,
    var showStatus: Boolean = false,
    val userId:String,
    val onClickListener: (SentVideoModel) -> Unit,
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.video_item, parent, false)
        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = videos[position]
        (holder as VideoViewHolder).bind(item)
        holder.itemView.setOnClickListener {
            onClickListener(item)
        }
    }

    override fun getItemCount(): Int {
        return videos.size
    }

    inner class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: SentVideoModel) {
            itemView.videoName.text = item.videoName
            if (!showStatus) {
                itemView.videoStatus.visibility = GONE
                itemView.seperator.visibility = GONE
            } else {
                itemView.videoStatus.visibility = VISIBLE
                itemView.seperator.visibility = VISIBLE
            }
            itemView.duration.text = item.duration
            itemView.sentTime.text = item.sentTime
            setThumbnails(item)
        }

        private fun setThumbnails(item: SentVideoModel){
            val reference = FirebaseStorageHelper.getVideoStorageReference(item.videoName,userId)
            GlideApp
                .with(itemView.context)
                .load(reference)
                .transform(
                    CenterCrop(),
                    GranularRoundedCorners(8f, 8f, 8f, 8f)
                )
                .into(itemView.videoThumbnail)
        }
    }
}