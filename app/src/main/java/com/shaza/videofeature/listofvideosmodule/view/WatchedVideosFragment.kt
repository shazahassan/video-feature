package com.shaza.videofeature.listofvideosmodule.view

import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.shaza.videofeature.R
import com.shaza.videofeature.listofvideosmodule.view.adapter.VideosAdapter
import com.shaza.videofeature.listofvideosmodule.viewmodel.WatchedVideosViewModel
import com.shaza.videofeature.playvideomodule.view.PlayVideoActivity
import com.shaza.videofeature.receiver.ConnectionChangeReceiver
import com.shaza.videofeature.receiver.OnConnected
import com.shaza.videofeature.sharedmodule.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.watched_videos_layout.*

@AndroidEntryPoint
class WatchedVideosFragment : BaseFragment(), OnConnected {

    companion object {
        fun newInstance() = WatchedVideosFragment()
    }

    private val viewModel: WatchedVideosViewModel by viewModels()
    private lateinit var receiver: ConnectionChangeReceiver
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.watched_videos_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.extractData(arguments)
        initObserver()

        val connectionListener = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        receiver = ConnectionChangeReceiver()
        receiver.onConnected = this
        activity?.registerReceiver(receiver, connectionListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(receiver)
    }

    private fun initObserver() {
        viewModel.workerId.observe(viewLifecycleOwner, {
            if (it != null) {
                viewModel.listenToVideos()
            }
        })

        viewModel.videos.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty()) {
                noWatchedVideos.visibility = View.VISIBLE
                watchedVideoList.visibility = View.GONE
            } else {
                noWatchedVideos.visibility = View.GONE
                watchedVideoList.visibility = View.VISIBLE
                val adapter = VideosAdapter(it, userId = viewModel.workerId.value!!) { video ->
                    startActivity(
                        PlayVideoActivity.newInstance(
                            requireActivity(),
                            viewModel.workerId.value!!,
                            video.videoName
                        )
                    )

                }
                watchedVideoList.adapter = adapter
            }
        })

        loadingObserver()

        errorObserver()
    }


    private fun loadingObserver() {
        viewModel.showLoadingIndicator.observe(viewLifecycleOwner, {
            handleLoadingIndicator(it)
        })
    }

    private fun errorObserver() {
        viewModel.errMsg.observe(viewLifecycleOwner, {
            handleErrMsg(it)
        })
    }

    override fun requestData() {
        viewModel.listenToVideos()
    }
}