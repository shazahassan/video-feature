package com.shaza.videofeature.listofvideosmodule.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.shaza.videofeature.MainActivity
import com.shaza.videofeature.R
import com.shaza.videofeature.listofvideosmodule.view.adapter.PageAdapter
import com.shaza.videofeature.listofvideosmodule.viewmodel.ListOfVideosViewModel
import com.shaza.videofeature.model.HealthWorkerModel
import com.shaza.videofeature.sharedmodule.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.list_of_videos_fragment.*
import kotlinx.android.synthetic.main.toolbar.*

@AndroidEntryPoint
class ListOfVideosFragment : BaseFragment() {

    companion object {
        const val healthWorkerKey = "HEALTH_WORKER"
        const val workerIdKey = "WORKER_ID"
        fun newInstance(healthWorkerModelInsideSupervisor: HealthWorkerModel): ListOfVideosFragment {
            val fragment = ListOfVideosFragment()
            val bundle = Bundle()
            bundle.putSerializable(healthWorkerKey, healthWorkerModelInsideSupervisor)
            fragment.arguments = bundle
            return fragment
        }
    }

    val viewModel: ListOfVideosViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_of_videos_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.extractData(this.arguments)
        initToolbar()
        initObservers()
    }

    private fun initToolbar() {
        toolbar.navigationIcon = requireContext().getDrawable(R.drawable.abc_vector_test)
        toolbar.setNavigationOnClickListener {
            (requireActivity() as MainActivity).onBackPressed()
        }
    }

    private fun setViewPager(
        workerId: String
    ) {
        viewPager.adapter = PageAdapter(childFragmentManager, workerId)
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun initObservers() {

        healthWorkerObserver()
    }

    private fun healthWorkerObserver() {
        viewModel.healthWorker.observe(viewLifecycleOwner, {
            toolbar.title = it.name
            setViewPager(it.userId)
        })
    }

}