package com.shaza.videofeature.listofvideosmodule.view

import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.shaza.videofeature.R
import com.shaza.videofeature.listofvideosmodule.view.adapter.VideosAdapter
import com.shaza.videofeature.listofvideosmodule.viewmodel.NewVideosViewModel
import com.shaza.videofeature.playvideomodule.view.PlayVideoActivity
import com.shaza.videofeature.receiver.ConnectionChangeReceiver
import com.shaza.videofeature.receiver.OnConnected
import com.shaza.videofeature.sharedmodule.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.new_videos_layout.*

@AndroidEntryPoint
class NewVideosFragment : BaseFragment(), OnConnected {

    companion object {
        fun newInstance() = NewVideosFragment()
    }

    private val viewModel: NewVideosViewModel by viewModels()

    private lateinit var receiver: ConnectionChangeReceiver
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.new_videos_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.extractData(arguments)
        initObserver()


        val connectionListener = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        receiver = ConnectionChangeReceiver()
        receiver.onConnected = this
        activity?.registerReceiver(receiver, connectionListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(receiver)
    }

    private fun initObserver() {
        viewModel.workerId.observe(viewLifecycleOwner, {
            if (it != null) {
                viewModel.listenToVideos()
            }
        })

        viewModel.videos.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty()) {
                noNewVideos.visibility = VISIBLE
                newVideosList.visibility = GONE
            } else {
                noNewVideos.visibility = GONE
                newVideosList.visibility = VISIBLE
                val adapter = VideosAdapter(it, userId = viewModel.workerId.value!!) { video ->
                    startActivity(
                        PlayVideoActivity.newInstance(
                            requireActivity(),
                            viewModel.workerId.value!!,
                            video.videoName
                        )
                    )

                }
                newVideosList.adapter = adapter
            }
        })

        loadingObserver()
        errorObserver()
    }


    private fun loadingObserver() {
        viewModel.showLoadingIndicator.observe(viewLifecycleOwner, {
            handleLoadingIndicator(it)
        })
    }

    private fun errorObserver() {
        viewModel.errMsg.observe(viewLifecycleOwner, {
            handleErrMsg(it)
        })
    }

    override fun requestData() {
        viewModel.listenToVideos()
    }
}