package com.shaza.videofeature.listofvideosmodule.view.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.shaza.videofeature.listofvideosmodule.view.ListOfVideosFragment
import com.shaza.videofeature.listofvideosmodule.view.NewVideosFragment
import com.shaza.videofeature.listofvideosmodule.view.WatchedVideosFragment


class PageAdapter(
    fm: FragmentManager,
    private val workerId: String
) : FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                val bundle = Bundle()
                val newVideosFragment = NewVideosFragment()
                bundle.putString(ListOfVideosFragment.workerIdKey, workerId)
                newVideosFragment.arguments = bundle
                newVideosFragment
            }
            1 -> {
                val bundle = Bundle()
                val watchedVideosFragment = WatchedVideosFragment()
                bundle.putString(ListOfVideosFragment.workerIdKey, workerId)
                watchedVideosFragment.arguments = bundle
                watchedVideosFragment
            }
            else -> {
                val bundle = Bundle()
                val newVideosFragment = NewVideosFragment()
                bundle.putString(ListOfVideosFragment.workerIdKey, workerId)
                newVideosFragment.arguments = bundle
                newVideosFragment
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> {
                return "New videos"
            }
            1 -> {
                return "Watched videos"
            }
        }
        return super.getPageTitle(position)
    }

}