package com.shaza.videofeature.listofvideosmodule.viewmodel

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.listofvideosmodule.model.repo.VideoRepo
import com.shaza.videofeature.listofvideosmodule.view.ListOfVideosFragment
import com.shaza.videofeature.model.HealthWorkerModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ListOfVideosViewModel @Inject constructor(
    application: Application,
    private val repo: VideoRepo
) : AndroidViewModel(
    application
) {

    private val tag = this::class.java.simpleName

    val healthWorker = MutableLiveData<HealthWorkerModel>()

    fun extractData(argument: Bundle?) {
        healthWorker.value =
            argument?.getSerializable(ListOfVideosFragment.healthWorkerKey) as HealthWorkerModel?
    }

}