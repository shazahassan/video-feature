package com.shaza.videofeature.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.shaza.videofeature.helper.internetconnectionhelper.InternetConnectionHelper


/**
 * Created by Shaza Hassan on 02-Dec-2021
 */

class ConnectionChangeReceiver : BroadcastReceiver() {
    var onConnected: OnConnected? = null
    override fun onReceive(context: Context, intent: Intent?) {

        val isConnected = InternetConnectionHelper.isConnected(context)
        if (isConnected) {
            if (onConnected != null) {
                onConnected?.requestData()
            }
            Toast.makeText(context, "Internet Connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show()

        }

    }
}

interface OnConnected {
    fun requestData()
}