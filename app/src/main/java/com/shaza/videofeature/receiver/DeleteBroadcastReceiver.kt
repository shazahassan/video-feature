package com.shaza.videofeature.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.shaza.videofeature.sharedmodule.AppConstants
import java.io.File

/**
 * Created by Shaza Hassan on 29-Nov-2021
 */

class DeleteBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action
        if (action.equals(AppConstants.deleteBroadcastReceiver)) {
            val filePath = intent?.extras?.getString(AppConstants.inputFileKey)
            filePath?.let { deleteFile(it) }
            val intent = Intent()
            intent.action = AppConstants.updateFileStatusBroadcastReceiver
            intent.putExtra(AppConstants.updateCompressedFile, true)
            intent.putExtra(AppConstants.inputFileKey, filePath)
            context?.applicationContext?.let {
                LocalBroadcastManager.getInstance(it).sendBroadcast(intent)
            }
        }
    }

    private fun deleteFile(filePath: String) {
        val file = File(filePath)
        if (file.exists()) {
            file.delete()
        }
    }
}