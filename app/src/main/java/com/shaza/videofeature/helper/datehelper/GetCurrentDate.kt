package com.shaza.videofeature.helper.datehelper

import java.text.SimpleDateFormat
import java.util.*

object GetCurrentDate {
    fun getCurrentDate(): String {
        val date = Calendar.getInstance().time
        val format = "MMM dd,yyyy"
        val locale = Locale.getDefault()
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(date)
    }

    fun getDate(milliSeconds: Long): String {
        val locale = Locale.getDefault()
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat("MMM dd,yyyy", locale)

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }
}