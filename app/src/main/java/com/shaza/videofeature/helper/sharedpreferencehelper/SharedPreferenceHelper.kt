package com.shaza.videofeature.helper.sharedpreferencehelper

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Shaza Hassan on 01-Dec-2021
 */
object SharedPreferenceHelper {

    private lateinit var sharedPreference: SharedPreferences

    fun initSharedPreferences(context: Context) {
        sharedPreference =
            context.getSharedPreferences("VIDEO_SHARED_PREFERENCES", Context.MODE_PRIVATE) ?: return
    }

    fun <T : Any> setValue(key: String, value: T) {
        with(sharedPreference.edit()) {
            when (value) {
                is String -> {
                    putString(key, value)
                    apply()
                }
                is Int -> {
                    putInt(key, value)
                    apply()
                }
                is Boolean -> {
                    putBoolean(key, value)
                    apply()
                }
                is Float -> {
                    putFloat(key, value)
                    apply()
                }
                is Long -> {
                    putLong(key, value)
                    apply()
                }
                else -> {
                    throw UnsupportedOperationException()
                }
            }
        }
    }

    fun <T : Any> getValue(key: String, defaultValue: T): T {
        return when (defaultValue) {
            is String -> sharedPreference.getString(key, defaultValue as String) as T
            is Int -> sharedPreference.getInt(key, defaultValue as Int) as T
            is Boolean -> sharedPreference.getBoolean(key, defaultValue as Boolean) as T
            is Float -> sharedPreference.getFloat(key, defaultValue as Float) as T
            is Long -> sharedPreference.getLong(key, defaultValue as Long) as T
            else -> throw UnsupportedOperationException("Not yet implemented")
        }
    }

    fun clearSharedPreferences() {
        with(sharedPreference.edit()) {
            clear()
            apply()
        }
    }

}