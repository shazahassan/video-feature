package com.shaza.videofeature.helper.storagehelper

import android.os.Environment
import android.os.StatFs

/**
 * Created by Shaza Hassan on 15-Nov-2021
 */
object StorageHelper {

    var Kb = (1 * 1024).toLong()
    var Mb = Kb * 1024
    var Gb = Mb * 1024

    var neededFreeSpace = (1.5 * Gb).toLong()

    fun getEmptySpaceInBytes(): Long {
        return getEnvironmentFolder().availableBytes
    }

    fun getEmptySpaceInReadableWay(): String {
        val space = getEnvironmentFolder().availableBytes
        return formatSpaceInReadableWay(space)
    }

    fun getTotalSpaceInBytes(): String {
        val space = getEnvironmentFolder().totalBytes
        return formatSpaceInReadableWay(space)
    }

    private fun getEnvironmentFolder(): StatFs {
        val path = Environment.getDataDirectory()
        return StatFs(path.path)
    }

    private fun formatSpaceInReadableWay(spaceInBytes: Long): String {
        var unitOfSpace = 0
        var showingSpace = spaceInBytes
        while (showingSpace >= 1024) {
            showingSpace /= 1024
            unitOfSpace++
        }
        val unit = unitOfSpace(unitOfSpace)
        return showingSpace.toString().take(4) + unit
    }

    private fun unitOfSpace(unitOfSpace: Int): String {
        return when (unitOfSpace) {
            0 -> {
                "Bytes"
            }
            1 -> {
                "KB"
            }
            2 -> {
                "MB"
            }
            else -> {
                "GB"
            }
        }
    }
}