package com.shaza.videofeature.helper.firebasestoragehelper

import android.net.Uri
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.google.firebase.storage.ktx.storage


/**
 * Created by Shaza Hassan on 16-Nov-2021
 */
object FirebaseStorageHelper {

    private val storageReference = Firebase.storage.reference

    fun getVideoStorageReference(fileName: String,userUid: String) :StorageReference{
        return storageReference.child("video/${userUid}/$fileName")
    }

    fun uploadFile(fileUri: Uri, userUid: String): UploadTask {
        val videoRef = storageReference.child("video/${userUid}/${fileUri.lastPathSegment}")
        return videoRef.putFile(fileUri)
    }

    fun getDownloadUrl(fileUri: Uri, userUid: String): StorageReference {
        return storageReference.child("video/${userUid}/${fileUri.lastPathSegment}")
    }

    fun getDownloadUrl(fileName: String, userUid: String): StorageReference {
        return storageReference.child("video/${userUid}/$fileName")
    }
}