package com.shaza.videofeature.helper.firebaseauthenticationhelper

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

/**
 * Created by Shaza Hassan on 18-Nov-2021
 */
object FirebaseAuthenticationHelper {
    private val auth = Firebase.auth

    fun signupUserWithEmailAndPassword(email: String, password: String): Task<AuthResult> {
        return auth.createUserWithEmailAndPassword(email, password)
    }

    fun currentUser(): FirebaseUser? {
        return auth.currentUser
    }

    fun loginWithEmailAndPassword(email: String, password: String): Task<AuthResult> {
        return auth.signInWithEmailAndPassword(email, password)
    }

    fun logout() {
        auth.signOut()
    }
}