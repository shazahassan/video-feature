package com.shaza.videofeature.helper.ffmpeghelper

import com.arthenica.mobileffmpeg.Config.RETURN_CODE_CANCEL
import com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS
import com.arthenica.mobileffmpeg.FFmpeg

/**
 * Created by Shaza Hassan on 27-Nov-2021
 */

object FFmpegHelper {

    fun execFFmpegBinary(
        command: String,
        onSuccessListener: () -> Unit,
        onProgress: (Long) -> Unit,
        onError: (Throwable) -> Unit
    ) {

        try {
            val executeID = FFmpeg.executeAsync(
                command
            ) { executionId, returnCode ->
                if (returnCode == RETURN_CODE_SUCCESS) {
                    onSuccessListener()
                } else if (returnCode == RETURN_CODE_CANCEL) {

                } else {
                }
            }
            onProgress(executeID)
        } catch (e: Exception) {
            onError(e)
        }
    }

    fun compressVideoCommand(inputFile: String, outputFile: String): String {
        return "-y " +
                "-i " +
                "'$inputFile'" +
                " -s " + "480x640" +
                " -r " + "60 " +
                "-vcodec " + "mpeg4 " +
                "-b:v " + "1000k " +
                "-b:a " + "48000 " +
                "-ac " + "2 " +
                "-ar " + "22050 " +
                "'$outputFile'"
    }

    fun trimVideoCommand(inputFile: String, startS: Int, endS: Int, outputFile: String): String {
        return "-y " +
                "-i " +
                "'$inputFile'" +
                " -ss ${startS}" +
                " -t " + "${(endS - startS)} " +
                "-c copy " +
                "'$outputFile'"
    }
}