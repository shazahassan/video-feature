package com.shaza.videofeature.helper.videotimehelper

object VideoTime {
    fun timeConversion(value: Int): String {
        val currentTime: String
        val mns = value / 60000 % 60000
        val scs = value % 60000 / 1000
        currentTime = String.format("%02d:%02d", mns, scs)
        return currentTime
    }
}