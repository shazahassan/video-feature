package com.shaza.videofeature.helper.workerhelper.compressworker

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.shaza.videofeature.sharedmodule.AppConstants
import java.io.File


/**
 * Created by Shaza Hassan on 27-Nov-2021
 */

class CompressWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {
    override fun doWork(): Result {

        val inputFile = inputData.getString(AppConstants.inputFileKey)
        val outputFile = inputData.getString(AppConstants.outputFileKey)

        return if (inputFile.isNullOrEmpty() && outputFile.isNullOrEmpty()) {
            Result.failure()
        } else {
//            val command = FFmpegHelper.compressVideoCommand(inputFile!!, outputFile!!)
//            FFmpegHelper.execFFmpegBinary(command) {
//                val intent = Intent(applicationContext, DeleteBroadcastReceiver::class.java)
//                intent.action = AppConstants.deleteBroadcastReceiver
//                intent.putExtra(AppConstants.inputFileKey, inputFile)
//                applicationContext.sendBroadcast(intent)
//            }
            Result.success()
        }

    }

    fun deleteFile(filePath: String) {
        val file = File(filePath)
        if (file.exists()) {
            file.delete()
        }
    }
}