package com.shaza.videofeature.helper.firebasefirestorehelper

import android.annotation.SuppressLint
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.shaza.videofeature.model.SentVideoModel
import com.shaza.videofeature.signupmodule.model.UserModel

/**
 * Created by Shaza Hassan on 18-Nov-2021
 */
object FirebaseFirestoreHelper {

    private const val userCollection = "user"
    private const val videosCollection = "videos"

    @SuppressLint("StaticFieldLeak")
    private val database = Firebase.firestore

    fun saveUserInDB(user: UserModel, userUid: String): Task<Void> {
        return database.collection(userCollection).document(userUid).set(user)
    }

    fun getUserData(userId: String): Task<DocumentSnapshot> {
        return database.collection(userCollection).document(userId).get()
    }

    fun addVideoNameInDB(videoModel: SentVideoModel, userUid: String): Task<Void> {
        return database.collection(videosCollection).document(userUid).collection(videosCollection)
            .document(videoModel.videoName)
            .set(videoModel)
    }

    fun getAllVideosForHealthWorker(workerId: String): Query {
        return database.collection(videosCollection).document(workerId).collection(videosCollection)
            .orderBy("sent_time", Query.Direction.DESCENDING)
    }

    fun listenToNewVideos(workerId: String): Query {
        return database.collection(videosCollection).document(workerId).collection(videosCollection)
            .whereEqualTo("seen", false).orderBy("sent_time", Query.Direction.DESCENDING)
    }

    fun listenToWatchedVideo(workerId: String): Query {
        return database.collection(videosCollection).document(workerId).collection(videosCollection)
            .whereEqualTo("seen", true).orderBy("sent_time", Query.Direction.DESCENDING)
    }

    fun updateSeenStatus(videoName: String, workerId: String): Task<Void> {
        return database.collection(videosCollection).document(workerId).collection(videosCollection)
            .document(videoName)
            .update("seen", true)
    }
}