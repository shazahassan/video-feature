package com.shaza.videofeature

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.shaza.videofeature.homemodule.view.HomeFragment
import com.shaza.videofeature.loginmodule.view.LoginFragment
import com.shaza.videofeature.sharedmodule.permissionhelper.PermissionHelper
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : PermissionHelper() {

    private val viewModel: MainActivityVM by viewModels()

    private val _tag = this::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.checkIfThereIsLoggedInUser()
        handleAuthenticatedUser()
    }

    private fun handleAuthenticatedUser() {
        viewModel.isLoggedInUser.observe(this, {
            if (it) {
                viewModel.userUid?.let { userId -> HomeFragment.newInstance(userId) }
                    ?.let { fragment -> goToFragment(fragment) }
            } else {
                goToFragment(LoginFragment.newInstance(), false)
            }
        })
    }

    fun goToFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        val fragmentManager: FragmentManager = supportFragmentManager
        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        ft.add(R.id.fragment_container_view, fragment, fragment.tag)
        if (addToBackStack) {
            ft.addToBackStack(null)
        }
        ft.commitAllowingStateLoss()
    }

    fun removePreviousFragment() {
        val fm: FragmentManager = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            pop()
        }
        for (fragment in fm.fragments) {
            supportFragmentManager.beginTransaction().remove(fragment!!).commit()
        }
    }

    fun pop() {
        supportFragmentManager.popBackStack()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            pop()
        else
            super.onBackPressed()
    }
}