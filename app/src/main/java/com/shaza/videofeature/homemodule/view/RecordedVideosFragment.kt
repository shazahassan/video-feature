package com.shaza.videofeature.homemodule.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.shaza.videofeature.R
import com.shaza.videofeature.homemodule.view.adapter.LocalVideoListAdapter
import com.shaza.videofeature.homemodule.viewmodel.VideoViewModel
import kotlinx.android.synthetic.main.recorded_video_fragment.*

class RecordedVideosFragment : Fragment() {

    companion object {
        fun newInstance() = RecordedVideosFragment()
    }

    private val viewModel: VideoViewModel by viewModels({ requireParentFragment() })

    lateinit var videoAdapter: LocalVideoListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recorded_video_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleShowingLocallyFile()
        listOfAllFiles()
        viewModel.getAllVideoLocally()
    }

    private fun listOfAllFiles() {
        videoAdapter =
            LocalVideoListAdapter(mutableListOf(), mutableListOf(), mutableListOf()) {
                return@LocalVideoListAdapter viewModel.uploadVideo(it)
            }
        localVideoList.adapter = videoAdapter
    }

    private fun handleShowingLocallyFile() {
        viewModel.listOfVideosWithoutCompressedLiveData.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                viewModel.compressingVideo()
            }
        })

        viewModel.listOfVideosInCompressingLiveData.observe(viewLifecycleOwner, {
            videoAdapter.compressingList = it
            videoAdapter.notifyDataSetChanged()
            showOrHideLocalList()
        })

        viewModel.listOfCompressedVideoLiveData.observe(viewLifecycleOwner, {
            videoAdapter.needToUpload = it
            videoAdapter.notifyDataSetChanged()
            showOrHideLocalList()

        })

        viewModel.listOfFileInUploadingLiveData.observe(viewLifecycleOwner, {
            videoAdapter.uploadingList = it
            videoAdapter.notifyDataSetChanged()
            showOrHideLocalList()
        })
    }

    private fun showOrHideLocalList() {
        if (videoAdapter.itemCount == 0) {
            noLocalVideos.visibility = VISIBLE
            localVideoList.visibility = GONE
        } else {
            noLocalVideos.visibility = GONE
            localVideoList.visibility = VISIBLE
        }
    }
}