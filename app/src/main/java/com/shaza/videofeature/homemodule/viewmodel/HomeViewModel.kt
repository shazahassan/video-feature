package com.shaza.videofeature.homemodule.viewmodel

import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.homemodule.model.repo.HomeRepo
import com.shaza.videofeature.homemodule.view.HomeFragment
import com.shaza.videofeature.signupmodule.model.UserModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


@HiltViewModel
open class HomeViewModel @Inject constructor(
    application: Application,
    private val repo: HomeRepo
) : AndroidViewModel(application) {

    private val tag = this::class.java.simpleName

    val userUid = MutableLiveData<String>()
    val compositeDisposable = CompositeDisposable()

    val errMsg = MutableLiveData<String>()
    val showLoadingIndicator = MutableLiveData<Boolean>()
    val user = MutableLiveData<UserModel>()

    fun extractData(arguments: Bundle?) {
        userUid.value = arguments?.getString(HomeFragment.userIdKey)
    }

    fun getUserData() {
        if (userUid.value != null) {
            getUserDataFromFireStore()
        } else {
            getLocalUserData()
        }
    }

    private fun getLocalUserData() {
        user.value = repo.getUserDataInSharedPreferences()
    }

    private fun getUserDataFromFireStore() {
        val observable = repo.getUserData(userUid.value!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                showLoadingIndicator.postValue(true)
            }
            .doOnNext {
                user.postValue(it)
            }
            .doOnError {
                errMsg.postValue(it.message)
            }
            .doOnComplete {
                showLoadingIndicator.postValue(false)
            }

        compositeDisposable.add(observable.subscribe(
            { Log.v(tag, "Get user data") },
            { Log.v(tag, "failed get user") }
        ))
    }

    fun logout() {
        repo.logout()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}