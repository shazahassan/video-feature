package com.shaza.videofeature.homemodule.model.repo

import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.storage.ktx.component1
import com.google.firebase.storage.ktx.component2
import com.shaza.videofeature.helper.ffmpeghelper.FFmpegHelper
import com.shaza.videofeature.helper.firebaseauthenticationhelper.FirebaseAuthenticationHelper
import com.shaza.videofeature.helper.firebasefirestorehelper.FirebaseFirestoreHelper
import com.shaza.videofeature.helper.firebasestoragehelper.FirebaseStorageHelper
import com.shaza.videofeature.helper.sharedpreferencehelper.SharedPreferenceHelper
import com.shaza.videofeature.homemodule.model.ProgressModel
import com.shaza.videofeature.homemodule.model.SupervisorModel
import com.shaza.videofeature.model.SentVideoModel
import com.shaza.videofeature.model.VideoModel
import com.shaza.videofeature.sharedmodule.AppConstants
import com.shaza.videofeature.signupmodule.model.UserModel
import io.reactivex.subjects.PublishSubject

/**
 * Created by Shaza Hassan on 22-Nov-2021
 */

class HomeRepo {

    fun getUserData(userUid: String): PublishSubject<UserModel> {
        val publishSubject: PublishSubject<UserModel> = PublishSubject.create()
        FirebaseFirestoreHelper.getUserData(userUid).addOnSuccessListener { task ->
            if (task.exists()) {
                var user = task.toObject<UserModel>()
                if (user?.role == "Supervisor") {
                    user = task.toObject<SupervisorModel>()
                }
                user?.userId = userUid
                user?.let { saveUserDataInSharedPreferences(it) }
                user?.let { publishSubject.onNext(it) }
                publishSubject.onComplete()

            }
        }.addOnFailureListener {
            publishSubject.onError(it)
        }
        return publishSubject
    }

    fun uploadFile(videoFile: VideoModel, userUid: String): PublishSubject<VideoModel> {
        val publishSubject: PublishSubject<VideoModel> = PublishSubject.create()
        FirebaseStorageHelper.uploadFile(videoFile.fileUri!!, userUid)
            .addOnProgressListener { (bytesTransferred, totalByteCount) ->
                videoFile.progressModel = ProgressModel(bytesTransferred, totalByteCount)
                publishSubject.onNext(
                    videoFile
                )

            }
            .addOnSuccessListener {
                FirebaseStorageHelper.getDownloadUrl(
                    videoFile.fileUri!!,
                    userUid
                ).downloadUrl.addOnCompleteListener {
                    if (it.isSuccessful) {
                        videoFile.downloadUrl = it.result.toString()
                        publishSubject.onNext(
                            videoFile
                        )
                        publishSubject.onComplete()
                    } else {
                        it.exception?.let { exception -> publishSubject.onError(exception) }
                    }
                }
            }.addOnFailureListener {
                publishSubject.onError(it)
            }

        return publishSubject
    }

    fun saveVideoNameInDB(
        sentVideoModel: SentVideoModel,
        userUid: String
    ): PublishSubject<Boolean> {
        val publishSubject: PublishSubject<Boolean> = PublishSubject.create()
        FirebaseFirestoreHelper.addVideoNameInDB(sentVideoModel, userUid).addOnSuccessListener {
            publishSubject.onNext(true)
            publishSubject.onComplete()
        }.addOnFailureListener {
            publishSubject.onError(it)
        }
        return publishSubject
    }

    private fun saveUserDataInSharedPreferences(userModel: UserModel) {
        SharedPreferenceHelper.setValue(AppConstants.sharedPreferencesNameKey, userModel.name)
        SharedPreferenceHelper.setValue(AppConstants.sharedPreferencesUserIdKey, userModel.userId)
        SharedPreferenceHelper.setValue(AppConstants.sharedPreferencesRoleKey, userModel.role)
    }

    fun getUserDataInSharedPreferences(): UserModel {
        val userModel = UserModel()
        userModel.name = SharedPreferenceHelper.getValue(AppConstants.sharedPreferencesNameKey, "")
        userModel.userId =
            SharedPreferenceHelper.getValue(AppConstants.sharedPreferencesUserIdKey, "")
        userModel.role = SharedPreferenceHelper.getValue(AppConstants.sharedPreferencesRoleKey, "")
        return userModel
    }

    fun compressVideo(
        inputFilePath: String,
        outputFilePath: String,
        video: VideoModel
    ): PublishSubject<Any> {
        val publishSubject: PublishSubject<Any> = PublishSubject.create()
        val command = FFmpegHelper.compressVideoCommand(inputFilePath, outputFilePath)
        FFmpegHelper.execFFmpegBinary(command,
            { // on suc
                publishSubject.onNext(true)
            }, {
                // on progress
                video.executeId = it
                publishSubject.onNext(video)
            },
            {
                publishSubject.onError(it)
            })
        return publishSubject
    }

    fun logout() {
        FirebaseAuthenticationHelper.logout()
        SharedPreferenceHelper.clearSharedPreferences()
    }

}