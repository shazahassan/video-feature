package com.shaza.videofeature.homemodule.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.shaza.videofeature.R
import com.shaza.videofeature.homemodule.view.adapter.LocalVideoListAdapter
import com.shaza.videofeature.homemodule.viewmodel.VideoViewModel
import com.shaza.videofeature.listofvideosmodule.view.adapter.VideosAdapter
import kotlinx.android.synthetic.main.sent_videos_fragment_fragment.*

class SentVideosFragment : Fragment() {

    companion object {
        fun newInstance() = SentVideosFragment()
    }

    private val viewModel: VideoViewModel by viewModels({ requireParentFragment() })

    lateinit var videoAdapter: LocalVideoListAdapter
    lateinit var adapter: VideosAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sent_videos_fragment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = VideosAdapter(mutableListOf(), showStatus = true,viewModel.userUid.value!!) {}
        sentVideoList.adapter = adapter
        viewModel.getVideosForHealthWorker()
        handleShowingSentVideos()
    }

    private fun handleShowingSentVideos() {
        viewModel.listOfSentVideosLiveData.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty()) {
                noVideosUploaded.visibility = VISIBLE
                sentVideoList.visibility = GONE
            } else {
                noVideosUploaded.visibility = GONE
                sentVideoList.visibility = VISIBLE
                adapter.videos = it
                adapter.notifyDataSetChanged()
            }
        })
    }
}