package com.shaza.videofeature.homemodule.view.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.shaza.videofeature.R
import com.shaza.videofeature.model.VideoModel
import kotlinx.android.synthetic.main.upload_video_item.view.*
import kotlinx.android.synthetic.main.video_item.view.videoName
import kotlinx.android.synthetic.main.video_item.view.videoStatus
import java.math.RoundingMode
import com.bumptech.glide.Glide
import java.io.File
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions


/**
 * Created by Shaza Hassan on 23-Nov-2021
 */
class LocalVideoListAdapter(
    var uploadingList: MutableList<VideoModel>,
    var compressingList: MutableList<VideoModel>,
    var needToUpload: MutableList<VideoModel>,
    var onButtonClicked: (VideoModel) -> Boolean
) : RecyclerView.Adapter<ViewHolder>() {

    private val IN_UPLOADING = 0
    private val IN_COMPRESSING = 1
    private val NEED_TO_UPLOAD = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.upload_video_item, parent, false)
        return VideoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: VideoModel
        val status: String
        val pair = handleBindViewHolder(position, holder)
        item = pair.first
        status = pair.second

        holder.itemView.videoUpload.setOnClickListener {
            it.isEnabled = false
            val returnValue = onButtonClicked(item)
            if (returnValue) {
                holder.itemView.videoStatus.visibility = VISIBLE
                holder.itemView.videoUpload.visibility = GONE
            }
            it.isEnabled = true
        }

        (holder as VideoViewHolder).bindView(item, status)
    }

    private fun handleBindViewHolder(
        position: Int,
        holder: ViewHolder,
    ): Pair<VideoModel, String> {
        val item: VideoModel
        val status: String
        when {
            position < uploadingList.size -> {
                showVideoStatus(holder)
                val pair = handleVideoUploading(position)
                item = pair.first
                status = pair.second
            }
            position - uploadingList.size < compressingList.size -> {
                showVideoStatus(holder)
                val realPosition = position - uploadingList.size
                val pair = handleCompressingVideo(realPosition)
                item = pair.first
                status = pair.second
            }
            position - uploadingList.size - compressingList.size < needToUpload.size -> {
                val realPosition = position - compressingList.size - uploadingList.size
                item = needToUpload[realPosition]
                status = "Not Sent"
                showSendButton(holder)
            }
            else -> {
                item = VideoModel()
                status = "Not Sent"
                showVideoStatus(holder)
            }
        }
        return Pair(item, status)
    }

    private fun showSendButton(holder: ViewHolder) {
        holder.itemView.videoUpload.visibility = VISIBLE
    }

    private fun showVideoStatus(holder: ViewHolder) {
        holder.itemView.videoStatus.visibility = VISIBLE
        holder.itemView.videoUpload.visibility = GONE
    }

    private fun handleVideoUploading(
        realPosition: Int
    ): Pair<VideoModel, String> {
        val item = uploadingList[realPosition]
        val bytesTransferred = item.progressModel?.completed?.toDouble()
        var pre = ((bytesTransferred)?.div((item.progressModel?.total!!)))?.times(100)
        pre = pre?.toBigDecimal()?.setScale(1, RoundingMode.UP)?.toDouble()
        val status = "Uploading $pre%"
        return Pair(item, status)
    }

    private fun handleCompressingVideo(
        position: Int
    ): Pair<VideoModel, String> {
        val item = compressingList[position]
        var per: Float? = null
        if (item.progressModel?.completed!! <= item.progressModel?.total!!) {
            per = item.progressModel?.completed?.toFloat()?.div(item.progressModel!!.total)
                ?.times(100)
            per = per?.toBigDecimal()?.setScale(1, RoundingMode.UP)?.toFloat()

        }
        var status = "Compressing"

        if (per != null) {
            status += " $per%"
        }
        return Pair(item, status)
    }

    override fun getItemCount(): Int {
        return compressingList.size + uploadingList.size + needToUpload.size
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position < uploadingList.size -> {
                IN_UPLOADING
            }
            position - uploadingList.size < compressingList.size -> {
                IN_COMPRESSING
            }
            position - compressingList.size - uploadingList.size < needToUpload.size -> {
                NEED_TO_UPLOAD
            }
            else -> -1
        }
    }

    inner class VideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(item: VideoModel, status: String) {
            itemView.videoName.text = item.fileName
            itemView.sentTime.text = item.sentTime
            itemView.duration.text = item.duration
            itemView.videoStatus.text = status
            getThumbnails(item)
        }


        private fun getThumbnails(videoModel: VideoModel){
            val filePath = videoModel.filePath
            if (filePath != null){
                Glide
                    .with(itemView.context)
                    .load(Uri.fromFile(File(filePath)))
                    .transform(
                        CenterCrop(),
                        GranularRoundedCorners(8f, 8f, 8f, 8f)
                    )
                    .into(itemView.videoThumbnail)
            }
        }
    }
}