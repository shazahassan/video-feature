package com.shaza.videofeature.homemodule.view.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter


class PageAdapter(
    parentFragment: Fragment,
    private val fragmentList: List<Fragment>
) : FragmentStateAdapter(parentFragment) {
    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }
//    override fun getCount(): Int {
//        return 2
//    }
//
//    override fun getItem(position: Int): Fragment {
//        return when (position) {
//            0 -> {
//                val bundle = Bundle()
//                val newVideosFragment = RecordedVideosFragment()
////                bundle.putString(ListOfVideosFragment.workerIdKey, workerId)
////                newVideosFragment.arguments = bundle
//                newVideosFragment
//            }
//            1 -> {
//                val bundle = Bundle()
//                val watchedVideosFragment = SentVideosFragment()
////                bundle.putString(ListOfVideosFragment.workerIdKey, workerId)
////                watchedVideosFragment.arguments = bundle
//                watchedVideosFragment
//            }
//            else -> {
//                val bundle = Bundle()
//                val newVideosFragment = RecordedVideosFragment()
////                bundle.putString(ListOfVideosFragment.workerIdKey, workerId)
////                newVideosFragment.arguments = bundle
//                newVideosFragment
//            }
//        }
//    }
//
//    override fun getPageTitle(position: Int): CharSequence? {
//        when (position) {
//            0 -> {
//                return "Recorded videos"
//            }
//            1 -> {
//                return "Sent videos"
//            }
//        }
//        return super.getPageTitle(position)
//    }

}