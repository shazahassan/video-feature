package com.shaza.videofeature.homemodule.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shaza.videofeature.R
import com.shaza.videofeature.model.HealthWorkerModel
import kotlinx.android.synthetic.main.health_worker_item.view.*

/**
 * Created by Shaza Hassan on 23-Nov-2021
 */

class HealthWorkerAdapter(
    private val list: List<HealthWorkerModel>,
    val clickListener: (HealthWorkerModel) -> Unit
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.health_worker_item, parent, false)
        return HealthWorkerViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]
        (holder as HealthWorkerViewHolder).bind(item)
        holder.itemView.setOnClickListener {
            clickListener(item)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class HealthWorkerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: HealthWorkerModel) {
            itemView.workerName.text = item.name
        }
    }
}