package com.shaza.videofeature.homemodule.view

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayoutMediator
import com.shaza.videofeature.MainActivity
import com.shaza.videofeature.R
import com.shaza.videofeature.homemodule.model.SupervisorModel
import com.shaza.videofeature.homemodule.view.adapter.HealthWorkerAdapter
import com.shaza.videofeature.homemodule.view.adapter.PageAdapter
import com.shaza.videofeature.homemodule.viewmodel.VideoViewModel
import com.shaza.videofeature.listofvideosmodule.view.ListOfVideosFragment
import com.shaza.videofeature.loginmodule.view.LoginFragment
import com.shaza.videofeature.model.HealthWorkerModel
import com.shaza.videofeature.receiver.ConnectionChangeReceiver
import com.shaza.videofeature.receiver.OnConnected
import com.shaza.videofeature.sharedmodule.AppConstants
import com.shaza.videofeature.sharedmodule.BaseFragment
import com.shaza.videofeature.sharedmodule.Popup
import com.shaza.videofeature.sharedmodule.SnackBar
import com.shaza.videofeature.signupmodule.model.UserModel
import com.shaza.videofeature.videocropmodule.view.VideoCropFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.health_worker_layout.*
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.supervisor_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.video_name_popup.*

@AndroidEntryPoint
class HomeFragment : BaseFragment(), OnConnected {

    val _tag = this::class.java.simpleName

    lateinit var healthWorkerAdapter: HealthWorkerAdapter

    val viewModel: VideoViewModel by viewModels()

    companion object {
        const val userIdKey = "USER_ID_KEY"
        const val backToCompressKey = "BACK_TO_COMPRESS"
        const val startCompress = "START_COMPRESS"

        fun newInstance(userId: String): HomeFragment {
            val authenticatedFragment = HomeFragment()
            val bundle = Bundle()
            bundle.putString(userIdKey, userId)
            authenticatedFragment.arguments = bundle
            return authenticatedFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        viewModel.extractData(this.arguments)
        viewModel.getUserData()
        initObserver()
        initClickListener()
        initBroadcastReceiver()
        setFragmentResultListener(backToCompressKey) { _, result ->
            val startCompress = result.getBoolean(startCompress)
            if (startCompress) {
                viewModel.getAllVideoLocally()
            }
        }
    }

    private fun setViewPager() {
        val fragmentStateAdapter = listOf(RecordedVideosFragment(), SentVideosFragment())
        viewPager.adapter = PageAdapter(this, fragmentStateAdapter)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "Recorded videos"
                }
                1 -> {
                    tab.text = "Sent Videos"
                }
            }
        }.attach()
    }

    private fun initToolbar() {
        (requireActivity() as MainActivity).setSupportActionBar(toolbar)
    }

    private fun initObserver() {
        handleErrMsg()
        handleLoadingIndicator()
        userData()
    }

    private fun initClickListener() {
        startRecordingVideo.setOnClickListener {
            it.isEnabled = false
            checkPermission()
            it.isEnabled = true
        }
    }

    private fun initBroadcastReceiver() {
        val updateFileStatus = IntentFilter()
        updateFileStatus.addAction(AppConstants.updateFileStatusBroadcastReceiver)
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(updateFileStatusReceive, updateFileStatus)

        val connectionListener = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")
        val receiver = ConnectionChangeReceiver()
        receiver.onConnected = this
        activity?.registerReceiver(receiver, connectionListener)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.logout, menu)
        val item = menu.findItem(R.id.logout)
        item.actionView.setOnClickListener {
            onOptionsItemSelected(item)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {
                viewModel.logout()
                val activity = (requireActivity() as MainActivity)
                activity.removePreviousFragment()
                activity.goToFragment(LoginFragment())
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleLoadingIndicator() {
        viewModel.showLoadingIndicator.observe(viewLifecycleOwner, {
            if (it) {
                progress.visibility = VISIBLE
            } else {
                progress.visibility = GONE
            }
        })
    }

    private fun handleErrMsg() {
        viewModel.errMsg.observe(viewLifecycleOwner, {
            if (it != null) {
                errorPopup(it)
            }
        })
    }

    private fun userData() {
        viewModel.user.observe(viewLifecycleOwner, {
            toolbar.title = "Hi, ${it.name}"
            handleShowingUi(it)
        })
    }

    private fun handleShowingUi(user: UserModel) {
        if (user.role == getString(R.string.supervisor)) {
            if (user is SupervisorModel) {
                myHealthWorkerList(user.healthWorkers)
            }
            supervisorLayout.visibility = VISIBLE
            healthWorkerLayout.visibility = GONE
        } else if (user.role == getString(R.string.health_worker)) {
            healthWorkerLayout.visibility = VISIBLE
            supervisorLayout.visibility = GONE
            setViewPager()
        }
    }

    val captureVideoResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                if (viewModel.getVideoDuration(viewModel.videoPath) >= 2000) {
                    (requireActivity() as MainActivity).goToFragment(
                        VideoCropFragment.newInstance(
                            viewModel.videoPath
                        ), true
                    )
                } else {
                    Popup.show(
                        requireContext(),
                        "Info",
                        "Too Short video!\nPlease record video at least 2 sec",
                        "Okay",
                        null,
                        { dialogInterface, i ->
                            viewModel.checkIfNotNeedVideoIsCreated()
                            dialogInterface.dismiss()
                        }, null

                    )
                }
            } else {
                viewModel.checkIfNotNeedVideoIsCreated()
            }
        }

    override fun onPermissionGranted(permission: String) {
        if (permission == Manifest.permission.CAMERA) {
            showPopupWithTextField(requireContext())
        }
    }

    private val updateFileStatusReceive: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {

            val action = intent?.action
            if (action.equals(AppConstants.updateFileStatusBroadcastReceiver)) {
                val updateCompressed = intent?.extras?.getBoolean(AppConstants.updateCompressedFile)
                val filePath = intent?.extras?.getString(AppConstants.inputFileKey)
                if (filePath != null) {
                    viewModel.removeFromCompressingList(filePath)
                }

                if (updateCompressed == true) {
                    viewModel.getCompressedVideo()
                }
            }

        }
    }

    override fun requestData() {
        viewModel.getUserData()
    }

    private fun checkPermission() {
        when {
            ifCameraPermissionIsGranted() -> {
                showPopupWithTextField(requireContext())
            }

            shouldShowRequestPermission() -> {
                SnackBar.show(
                    requireView(),
                    "We need to camera permission",
                    Snackbar.LENGTH_INDEFINITE,
                    "Ok"
                ) {
                    requestListOfPermission.launch(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO
                        )
                    )
                }
            }

            else -> {
                requestListOfPermission.launch(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO
                    )
                )
            }
        }
    }

    private fun ifCameraPermissionIsGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED
    }

    private fun shouldShowRequestPermission(): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(
            requireActivity(),
            Manifest.permission.CAMERA
        )
    }

    private fun showPopupWithTextField(context: Context) {
        if (viewModel.checkFreeSpace()) {
            viewModel.createDefaultVideoName()
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.video_name_popup)
            val okayButton = dialog.okayButton
            val cancelButton = dialog.cancelButton
            dialog.sessionName.hint = "Dela session is: ${viewModel.videoName}"
            okayButton.setOnClickListener {
                val fileName = dialog.sessionName.text.toString()
                if (fileName.trim().isNotEmpty()) {
                    viewModel.updateFileName(fileName)
                }
                viewModel.createVideoFile()
                launchActivity()
                dialog.dismiss()
            }
            cancelButton.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
            val metrics = resources.displayMetrics

            val width: Int = metrics.widthPixels
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.window?.setLayout((6 * width) / 7, ViewGroup.LayoutParams.WRAP_CONTENT)

        } else {
            viewModel.showErrorNotEnoughSpace()
        }
    }

    private fun launchActivity() {
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 600)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, viewModel.videoUri)
        captureVideoResult.launch(intent)
    }

    private fun myHealthWorkerList(healthWorkers: List<HealthWorkerModel>) {
        if (healthWorkers.isNullOrEmpty()) {
            healthWorkerList.visibility = GONE
            noHealthWorker.visibility = VISIBLE
        } else {
            noHealthWorker.visibility = GONE
            healthWorkerList.visibility = VISIBLE
            healthWorkerAdapter = HealthWorkerAdapter(healthWorkers) {
                (requireActivity() as MainActivity).goToFragment(
                    ListOfVideosFragment.newInstance(it),
                    true
                )
            }
            healthWorkerList.layoutManager = LinearLayoutManager(requireContext())
            healthWorkerList.adapter = healthWorkerAdapter
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(requireContext())
            .unregisterReceiver(updateFileStatusReceive)
    }
}

