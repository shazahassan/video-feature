package com.shaza.videofeature.homemodule.viewmodel

import android.app.Application
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import com.arthenica.mobileffmpeg.Config
import com.shaza.videofeature.BuildConfig
import com.shaza.videofeature.R
import com.shaza.videofeature.VideoApplication
import com.shaza.videofeature.helper.datehelper.GetCurrentDate
import com.shaza.videofeature.helper.internetconnectionhelper.InternetConnectionHelper
import com.shaza.videofeature.helper.storagehelper.StorageHelper
import com.shaza.videofeature.helper.videotimehelper.VideoTime
import com.shaza.videofeature.homemodule.model.ProgressModel
import com.shaza.videofeature.homemodule.model.repo.HomeRepo
import com.shaza.videofeature.listofvideosmodule.model.repo.VideoRepo
import com.shaza.videofeature.model.SentVideoModel
import com.shaza.videofeature.model.VideoModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.IOException
import java.util.*
import javax.inject.Inject

/**
 * Created by Shaza Hassan on 06-Dec-2021
 */

@HiltViewModel
class VideoViewModel @Inject constructor(
    application: Application,
    private val repo: HomeRepo,
    private val videoRepo: VideoRepo
) : HomeViewModel(application, repo) {
    private val tag = this::class.java.simpleName

    var videoName: String = ""
    var videoPath: String = ""
    var videoUri: Uri? = null

    val listOfVideosWithoutCompressedLiveData = MutableLiveData<MutableList<VideoModel>>()
    private var listOfVideosWithoutCompressed = mutableListOf<VideoModel>()
    val listOfVideosInCompressingLiveData = MutableLiveData<MutableList<VideoModel>>()
    private var listOfVideosInCompressing = mutableListOf<VideoModel>()
    val listOfCompressedVideoLiveData = MutableLiveData<MutableList<VideoModel>>()
    private val listOfCompressedVideo = mutableListOf<VideoModel>()

    val listOfFileInUploadingLiveData = MutableLiveData<MutableList<VideoModel>>()
    private val listOfFileInUploading = mutableListOf<VideoModel>()

    val listOfSentVideosLiveData = MutableLiveData<MutableList<SentVideoModel>>()

    init {
        listOfVideosWithoutCompressedLiveData.value = mutableListOf()
        listOfCompressedVideoLiveData.value = mutableListOf()
        listOfVideosInCompressingLiveData.value = mutableListOf()
        listOfFileInUploadingLiveData.value = mutableListOf()
        if (!compressedDir().exists()) {
            compressedDir().mkdir()
        }
        Config.enableStatisticsCallback { statistics ->
            if (listOfVideosInCompressing.isNotEmpty()) {
                for (video in listOfVideosInCompressing) {
                    if (video.executeId == statistics.executionId) {
                        video.progressModel?.completed = statistics.time.toLong()
                        updateCompressingListWithProgress(video)
                    }
                }
            }
        }
    }

    fun createDefaultVideoName() {
        videoName = System.currentTimeMillis().toString()
    }

    fun updateFileName(fileName: String) {
        this.videoName = fileName
    }

    @Throws(IOException::class)
    fun createVideoFile() {
        // Create an image file name
        val storageDir = rootFile()
        val file = File(
            storageDir, // directory
            "$videoName.mp4"
        )
        // Save a file: path for use with ACTION_VIEW intents
        videoPath = file.absolutePath
        videoUri = FileProvider.getUriForFile(
            Objects.requireNonNull(getApplication<VideoApplication>()),
            BuildConfig.APPLICATION_ID + ".fileprovider", file
        )
    }

    fun checkFreeSpace(): Boolean {
        return StorageHelper.getEmptySpaceInBytes() > StorageHelper.neededFreeSpace
    }

    fun showErrorNotEnoughSpace() {
        errMsg.value = "You need at least 1.5GB free space"
    }

    fun getAllVideoLocally() {
        getNotCompressedVideos()

        getCompressedVideo()
    }

    fun getNotCompressedVideos() {
        val storageDir = rootFile()
        for (file in storageDir.listFiles().asList()) {
            val videoModel = VideoModel()
            val notAddedBefore = !listOfVideosWithoutCompressed.any { file.name == it.fileName }
            val fileNotInCompressing = !listOfVideosInCompressing.any { file.name == it.fileName }
            if (file.isFile && notAddedBefore && fileNotInCompressing) {
                videoModel.fileName = file.name
                videoModel.filePath = file.absolutePath
                videoModel.fileUri = file.toUri()
                val videoDuration = getVideoDuration(file.path)
                videoModel.duration = VideoTime.timeConversion(videoDuration)
                videoModel.sentTime = GetCurrentDate.getDate(file.lastModified())
                listOfVideosWithoutCompressed.add(videoModel)
            }
        }
        listOfVideosWithoutCompressedLiveData.value = listOfVideosWithoutCompressed
    }

    fun getCompressedVideo() {
        if (compressedDir().listFiles() != null) {
            for (file in compressedDir().listFiles().asList()) {
                val fileNotInTheList = !listOfCompressedVideo.any { file.name == it.fileName }
                val fileNotInUploading = !listOfFileInUploading.any { file.name == it.fileName }
                val fileInNotTempFile = !listOfVideosInCompressing.any { file.name == it.fileName }
                if (file.isFile && fileNotInTheList && fileNotInUploading && fileInNotTempFile) {
                    val videoModel = VideoModel()
                    videoModel.fileUri = file.toUri()
                    videoModel.filePath = file.absolutePath
                    videoModel.fileName = file.name
                    val videoDuration = getVideoDuration(file.path)
                    videoModel.duration = VideoTime.timeConversion(videoDuration)
                    videoModel.sentTime = GetCurrentDate.getDate(file.lastModified())
                    listOfCompressedVideo.add(videoModel)
                }
            }
            listOfCompressedVideoLiveData.value = listOfCompressedVideo
        }
    }

    private fun checkInternetConnection(): Boolean {
        return InternetConnectionHelper.isConnected(getApplication())
    }

    fun uploadVideo(video: VideoModel): Boolean {
        return when (userUid.value) {
            null -> {
                errMsg.value =
                    getApplication<VideoApplication>().getString(R.string.not_authorized_user)
                logout()
                false
            }
            else -> {
                return when (checkInternetConnection()) {
                    true -> {
                        startUploadVideo(video)
                        true
                    }
                    else -> {
                        errMsg.value =
                            getApplication<VideoApplication>().getString(R.string.internet_connection_error)
                        false
                    }
                }
            }
        }
    }

    private fun startUploadVideo(video: VideoModel) {
        val fileNotInUploading = !listOfFileInUploading.any { video.fileName == it.fileName }
        val fileNotInCompressing = !listOfVideosInCompressing.any { it.fileName == video.fileName }

        if (fileNotInUploading && fileNotInCompressing) {
            if (video.fileUri != null) {
                if (listOfCompressedVideo.contains(video)) {
                    listOfCompressedVideo.remove(video)
                    listOfCompressedVideoLiveData.value = listOfCompressedVideo
                }
                listOfFileInUploading.add(video)
                updateLiveDataListOfUploadingVideos()
                val observable = repo.uploadFile(video, userUid.value!!)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe {
                    }.doOnNext {
                        onNextUploadVideo(it, video)
                    }.doOnError {
                        onErrorUploadVideo(video, it)
                    }

                compositeDisposable.add(observable.subscribe(
                    { Log.v(tag, "video Uploading") },
                    { Log.v(tag, "failed Uploading video") }
                ))
            }
        }
    }

    private fun onNextUploadVideo(
        it: VideoModel,
        video: VideoModel,
    ) {
        if (it.downloadUrl != null) {
            val videoFile = File(video.filePath)
            saveVideoInDB(videoFile)
        } else {
            val index = listOfFileInUploading.indexOfFirst { video ->
                video.fileName == it.fileName
            }
            if (index != -1) {
                listOfFileInUploading[index] = it
                updateLiveDataListOfUploadingVideos()
            }
        }
    }

    private fun updateLiveDataListOfUploadingVideos() {
        listOfFileInUploadingLiveData.postValue(listOfFileInUploading)
    }

    private fun onErrorUploadVideo(video: VideoModel, it: Throwable) {
        removeItemFromUploadingList(video)
        errMsg.postValue(it.message)
    }

    private fun removeItemFromUploadingList(videoModel: VideoModel) {
        listOfFileInUploading.remove(videoModel)
        this.listOfFileInUploadingLiveData.postValue(listOfFileInUploading)
    }

    private fun saveVideoInDB(videoFile: File) {
        val videoDuration = getVideoDuration(videoFile.path)
        val sentVideoModel = SentVideoModel()
        sentVideoModel.videoName = videoFile.name
        sentVideoModel.duration = VideoTime.timeConversion(videoDuration)
        val observable = repo.saveVideoNameInDB(sentVideoModel, userUid.value!!)
            .subscribeOn(Schedulers.io())
            .doOnNext {
                onNextSaveVideoInDB(it, videoFile)
            }
            .doOnError {
                errMsg.postValue(it.message)
            }

        compositeDisposable.add(
            observable.subscribe(
                { Log.v(tag, "added to DB") },
                { Log.e(tag, "Failed with: ${it.message}") }
            )
        )
    }

    private fun onNextSaveVideoInDB(
        uploaded: Boolean,
        videoFile: File
    ) {
        if (uploaded) {
            showLoadingIndicator.postValue(false)

            deleteUploadedFile(videoFile)
            val video = listOfFileInUploading.find { videoModel ->
                videoFile.name == videoModel.fileName
            }
            video?.let { video -> removeItemFromUploadingList(video) }
        }
    }

    private fun deleteUploadedFile(file: File) {
        deleteFile(file)
        getAllVideoLocally()
    }

    private fun deleteFile(file: File) {
        if (file.exists()) {
            if (file.delete()) {
                Log.v(tag, "file Deleted :" + file.path)
            } else {
                Log.v(tag, "file not Deleted :" + file.path)
            }
        }
    }

    private fun getFileFromUri(fileUri: Uri): File {
        val storageDir = rootFile()
        val path = fileUri.lastPathSegment
        return File(storageDir, path)
    }

    fun checkIfNotNeedVideoIsCreated() {
        val storageDir = rootFile()
        val file = File(storageDir, "$videoName.mp4")
        if (file.exists()) {
            deleteFile(file)
        }
    }

    private fun rootFile() =
        File(getApplication<VideoApplication>().getExternalFilesDir(null)?.absolutePath)

    private fun compressedDir() = File(rootFile(), "/compressed")

    fun compressingVideo() {
        if (!listOfVideosWithoutCompressed.isNullOrEmpty()) {
            for (video in listOfVideosWithoutCompressed) {
                if (listOfVideosInCompressing.contains(video)) {
                    continue
                }
                listOfVideosInCompressing.add(video)
                listOfVideosInCompressingLiveData.value = listOfVideosInCompressing
                val duration: Int = getVideoDuration(video.filePath)
                if (duration != -1) {
                    startCompressVideo(duration, video)
                } else {
                    deleteFile(File(video.filePath))
                    getNotCompressedVideos()
                }
            }
        }
    }

    fun getVideoDuration(video: String?): Int {
        if (video != null) {
            val mp: MediaPlayer = MediaPlayer.create(getApplication(), Uri.parse(video))
            val duration: Int = mp.duration
            mp.release()
            return duration
        } else {
            return -1
        }

    }

    private fun startCompressVideo(totalVideoDuration: Int, video: VideoModel) {
        val progressModel = ProgressModel(0, totalVideoDuration.toLong())
        video.progressModel = progressModel
        updateCompressingListWithProgress(video)

        val outputFilePath = File(compressedDir(), video.fileName).absolutePath

        val observable = repo.compressVideo(
            video.filePath!!,
            outputFilePath,
            video
        ).subscribeOn(Schedulers.io())
            .doOnNext {
                onNextCompressingVideo(it, video)
            }
            .doOnError {
                errMsg.postValue(it.message)
            }

        compositeDisposable.add(
            observable.subscribe(
                { Log.v(tag, "compressing") },
                { Log.v(tag, it.message.toString()) }
            )
        )
    }

    private fun onCompleteVideoComplete(video: VideoModel) {
        removeFromCompressingList(video.filePath!!)
        getCompressedVideo()
    }

    private fun onNextCompressingVideo(
        nextValue: Any?,
        video: VideoModel
    ) {
        if (nextValue is VideoModel) {

        } else if (nextValue is Boolean) {
            if (nextValue) {
                onCompleteVideoComplete(video)
            }
        }
    }

    private fun updateCompressingListWithProgress(video: VideoModel) {
        val index = listOfVideosInCompressing.indexOfFirst { videoModel ->
            video.fileName == videoModel.fileName
        }
        if (index != -1) {
            listOfVideosInCompressing[index] = video
            listOfVideosInCompressingLiveData.postValue(listOfVideosInCompressing)
        }
    }

    fun removeFromCompressingList(filePath: String) {
        listOfVideosInCompressing = listOfVideosInCompressing.filter {
            it.filePath != filePath
        }.toMutableList()
        val indexOfFile = listOfVideosWithoutCompressed.indexOfFirst {
            filePath == it.filePath
        }
        listOfVideosWithoutCompressed.removeAt(indexOfFile)
        deleteFile(File(filePath))
        this.listOfVideosInCompressingLiveData.value = listOfVideosInCompressing
    }

    fun getVideosForHealthWorker() {
        val workerId = userUid.value
        if (workerId != null) {
            val observable = videoRepo.getListVideos(workerId)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    showLoadingIndicator.postValue(true)
                }
                .doOnNext {
                    showLoadingIndicator.postValue(false)

                    listOfSentVideosLiveData.postValue(it)
                }
                .doOnError {
                    errMsg.postValue(it.message)
                    showLoadingIndicator.postValue(false)
                }

            compositeDisposable.add(
                observable.subscribe(
                    { Log.v(tag, "data got") },
                    { Log.e(tag, it.message.toString()) }
                )
            )

        }
    }

}