package com.shaza.videofeature.homemodule.model

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */
data class ProgressModel(
    var completed: Long,
    var total: Long
)
