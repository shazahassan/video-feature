package com.shaza.videofeature.sharedmodule.permissionhelper

import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */

open class PermissionHelper : AppCompatActivity() {
    val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                onPermissionGranted()
            } else {
                onPermissionDenied()
            }
        }

    val requestListOfPermission =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            it.forEach { (key, value) -> println("$key = $value") }
        }

    open fun onPermissionGranted() {

    }

    open fun onPermissionDenied() {

    }
}