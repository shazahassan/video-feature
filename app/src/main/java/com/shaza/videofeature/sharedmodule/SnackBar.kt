package com.shaza.videofeature.sharedmodule

import android.view.View
import com.google.android.material.snackbar.Snackbar

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */
object SnackBar {

    fun show(
        view: View,
        msg: String,
        length: Int,
        actionMessage: CharSequence?,
        action: (View) -> Unit?
    ) {
        val snackBar = Snackbar.make(view, msg, length)
        if (actionMessage != null) {
            snackBar.setAction(actionMessage) {
                action(it)
            }.show()
        } else {
            snackBar.show()
        }
    }

}