package com.shaza.videofeature.sharedmodule

import com.shaza.videofeature.homemodule.model.repo.HomeRepo
import com.shaza.videofeature.listofvideosmodule.model.repo.VideoRepo
import com.shaza.videofeature.loginmodule.model.repo.LoginRepo
import com.shaza.videofeature.playvideomodule.model.repo.PlayVideoRepo
import com.shaza.videofeature.signupmodule.model.repo.SignupRepo
import com.shaza.videofeature.videocropmodule.model.repo.VideoCropRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */

@InstallIn(SingletonComponent::class)
@Module
object AppModule {

//    @Provides
//    fun provideUploadFileRepo() = UploadFileRepo()

    @Provides
    fun provideSignUpRepo() = SignupRepo()

    @Provides
    fun provideLoginRepo() = LoginRepo()

    @Provides
    fun provideHomeRepo() = HomeRepo()

    @Provides
    fun provideVideoRepo() = VideoRepo()

    @Provides
    fun providePlayVideoRepo() = PlayVideoRepo()

    @Provides
    fun provideTrimVideo() = VideoCropRepo()
}