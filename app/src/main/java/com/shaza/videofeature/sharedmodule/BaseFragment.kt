package com.shaza.videofeature.sharedmodule

import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.loading_view.*

/**
 * Created by Shaza Hassan on 23-Nov-2021
 */
open class BaseFragment : Fragment() {
    val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                onPermissionGranted()
            } else {
                onPermissionDenied()
            }
        }

    val requestListOfPermission =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            it.forEach { (key, value) ->
                if (value) {
                    onPermissionGranted(key)
                } else {
                    onPermissionDenied(key)
                }
            }
        }

    open fun onPermissionGranted() {

    }

    open fun onPermissionDenied() {

    }

    open fun onPermissionGranted(permission: String) {

    }

    open fun onPermissionDenied(permission: String) {

    }

    fun handleLoadingIndicator(showLoading: Boolean) {
        if (showLoading) {
            progress.visibility = View.VISIBLE
        } else {
            progress.visibility = View.GONE
        }

    }

    fun handleErrMsg(errorMsg: String) {
        errorPopup(errorMsg)
    }

    fun errorPopup(errMsg: String) {
        ErrorPopup.show(requireContext(), errMsg)
    }
}