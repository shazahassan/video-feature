package com.shaza.videofeature.sharedmodule

import android.content.Context
import com.shaza.videofeature.R

/**
 * Created by Shaza Hassan on 22-Nov-2021
 */
object ErrorPopup {
    fun show(context: Context, errorMsg: String) {
        Popup.show(
            context,
            context.getString(R.string.error_popup_title),
            errorMsg,
            context.getString(R.string.popup_ok_button),
            null,
            { dialogInterface, _ ->
                dialogInterface.dismiss()
            },
            null
        )
    }
}