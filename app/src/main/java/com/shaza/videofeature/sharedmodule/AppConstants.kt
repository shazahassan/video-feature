package com.shaza.videofeature.sharedmodule

/**
 * Created by Shaza Hassan on 29-Nov-2021
 */
object AppConstants {
    const val deleteBroadcastReceiver = "DELETE_BROADCAST_RECEIVER"
    const val updateFileStatusBroadcastReceiver = "UPDATE_FILE_STATUS_BROADCAST_RECEIVER"
    const val inputFileKey = "INPUT_FILE"
    const val outputFileKey = "OUTPUT_FILE"
    const val updateCompressedFile = "UPDATE_COMPRESSED_FILE"
    const val sharedPreferencesRoleKey = "ROLE_KEY"
    const val sharedPreferencesNameKey = "Name_KEY"
    const val sharedPreferencesUserIdKey = "USER_ID_KEY"
}