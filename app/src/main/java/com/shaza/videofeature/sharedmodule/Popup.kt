package com.shaza.videofeature.sharedmodule

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */
object Popup {

    fun show(
        context: Context,
        title: String?,
        message: String?,
        positiveActionString: String,
        negativeActionString: String?,
        positiveClickListener: DialogInterface.OnClickListener,
        negativeClickListener: DialogInterface.OnClickListener?,

        ) {
        val alertDialogBuilder = AlertDialog.Builder(context).create()
        with(alertDialogBuilder)
        {
            if (title != null) {
                setTitle(title)
            }

            setMessage(message)

            setButton(AlertDialog.BUTTON_POSITIVE, positiveActionString, positiveClickListener)
            if (negativeActionString != null && negativeClickListener != null) {
                setButton(
                    AlertDialog.BUTTON_NEGATIVE,
                    negativeActionString,
                    negativeClickListener
                )
            }

            show()
            setCanceledOnTouchOutside(false)

        }
    }
}