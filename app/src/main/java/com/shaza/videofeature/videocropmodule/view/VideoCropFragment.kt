package com.shaza.videofeature.videocropmodule.view

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.shaza.videofeature.MainActivity
import com.shaza.videofeature.R
import com.shaza.videofeature.helper.videotimehelper.VideoTime
import com.shaza.videofeature.homemodule.view.HomeFragment
import com.shaza.videofeature.sharedmodule.BaseFragment
import com.shaza.videofeature.sharedmodule.Popup
import com.shaza.videofeature.videocropmodule.viewmodel.VideoCropViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.play_video_activity.videoView
import kotlinx.android.synthetic.main.toolbar.*
import kotlinx.android.synthetic.main.video_crop_fragment.*

@AndroidEntryPoint
class VideoCropFragment : BaseFragment() {

    companion object {
        const val videoPathKey = "VIDEO_PATH_KEY"
        fun newInstance(videoPath: String): VideoCropFragment {
            val fragment = VideoCropFragment()
            val bundle = Bundle()
            bundle.putString(videoPathKey, videoPath)
            fragment.arguments = bundle
            return fragment
        }
    }

    private val viewModel: VideoCropViewModel by viewModels()

    private var values = mutableListOf<Float>()

    val r = Runnable { videoView.pause() }

    private var handler: Handler? = Handler(Looper.getMainLooper())

    lateinit var mediaController: MediaController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.video_crop_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.extractArguments(arguments)
        mediaController = MediaController(requireContext())
        initObservers()
        initListeners()
        initToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        handler?.removeCallbacks(r)
    }

    private fun initObservers() {
        viewModel.videoPath.observe(viewLifecycleOwner, {
            if (it != null) {
                videoView.setVideoURI(Uri.parse(it))
                videoView.setMediaController(mediaController)
                mediaController.requestFocus()
            }
        })

        viewModel.back.observe(viewLifecycleOwner, {
            if (it) {
                val bundle = Bundle()
                bundle.putBoolean(HomeFragment.startCompress, true)
                setFragmentResult(HomeFragment.backToCompressKey, bundle)
                (requireActivity() as MainActivity).pop()
            }
        })

        viewModel.showLoadingIndicator.observe(viewLifecycleOwner, {
            handleLoadingIndicator(it)
        })

        viewModel.errMsg.observe(viewLifecycleOwner, {
            handleErrMsg(it)
        })
    }

    @SuppressLint("SetTextI18n")
    private fun initListeners() {
        seekbarRange.addOnChangeListener { slider, value, fromUser ->
            if (fromUser) {
                handler?.removeCallbacks(r)
                values = slider.values
                val startValue = slider.values[0].times(1000).toInt()
                val endValue = slider.values[1].times(1000).toInt()
                videoView.start()
                videoView.seekTo(startValue)
                val valueOfDelay = endValue - startValue
                handler?.postDelayed(r, valueOfDelay.toLong())
                cropFrom.text = "crop from: ${VideoTime.timeConversion(startValue)}"
                cropTo.text = "crop to: ${VideoTime.timeConversion(endValue)}"
            }
        }

        cropVideo.setOnClickListener {
            videoView.pause()
            viewModel.trimVideo(values[0].toInt(), values[1].toInt())
        }

        delete.setOnClickListener {
            handleDeleteVideo()
        }

        videoView.setOnPreparedListener {
            val duration = videoView.duration

            if (duration >= 1000) {
                if (seekbarRange.valueTo != duration.div(1000).toFloat()) {
                    seekbarRange.valueFrom = 0f
                    cropFrom.text = "Crop from: ${VideoTime.timeConversion(0)}"
                    cropTo.text = "Crop to: ${VideoTime.timeConversion(duration)}"
                    seekbarRange.valueTo = duration.div(1000).toFloat()
                    values.add(0f)
                    values.add(duration.div(1000).toFloat())
                    seekbarRange.values = values
                }
                mediaController.setAnchorView(videoView)
                videoView.start()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        view!!.isFocusableInTouchMode = true
        view!!.requestFocus()
        view!!.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                handleDeleteVideo()
                // handle back button
                true
            } else false
        }
    }

    private fun initToolbar() {
        toolbar.title = "Crop video"
        toolbar.navigationIcon = requireContext().getDrawable(R.drawable.abc_vector_test)
        toolbar.setNavigationOnClickListener {
            handleDeleteVideo()
        }
    }

    private fun handleDeleteVideo() {
        Popup.show(
            requireContext(),
            "Warning",
            "Are you sure to delete this recorded video?",
            "Yes",
            "No",
            { dialogInterface, i ->
                viewModel.deleteVideoFile()
                dialogInterface.dismiss()
                (requireActivity() as MainActivity).pop()
            },
            { dialogInterface, i ->
                dialogInterface.dismiss()
            }

        )
    }
}