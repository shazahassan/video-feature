package com.shaza.videofeature.videocropmodule.model.repo

import com.shaza.videofeature.helper.ffmpeghelper.FFmpegHelper
import io.reactivex.subjects.PublishSubject

/**
 * Created by Shaza Hassan on 14-Dec-2021
 */
class VideoCropRepo {

    fun trimVideo(
        inputFilePath: String,
        startS: Int,
        endS: Int,
        outputPath: String
    ): PublishSubject<Any> {
        val publishSubject: PublishSubject<Any> = PublishSubject.create()
        val command = FFmpegHelper.trimVideoCommand(inputFilePath, startS, endS, outputPath)
        FFmpegHelper.execFFmpegBinary(command,
            { // on suc
                publishSubject.onNext(true)
                publishSubject.onComplete()
            }, {
                // on progress
                publishSubject.onNext("trimming")
            },
            {
                publishSubject.onError(it)
            })
        return publishSubject
    }
}