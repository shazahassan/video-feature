package com.shaza.videofeature.videocropmodule.viewmodel

import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.VideoApplication
import com.shaza.videofeature.videocropmodule.model.repo.VideoCropRepo
import com.shaza.videofeature.videocropmodule.view.VideoCropFragment
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.io.File
import javax.inject.Inject

@HiltViewModel
class VideoCropViewModel @Inject constructor(
    application: Application,
    private val repo: VideoCropRepo
) : AndroidViewModel(application) {

    val videoPath = MutableLiveData<String>()
    private val compositeDisposable = CompositeDisposable()
    private val tag = this::class.java.simpleName


    val errMsg = MutableLiveData<String>()
    val showLoadingIndicator = MutableLiveData<Boolean>()
    val back = MutableLiveData<Boolean>()

    private fun rootFile() =
        File(getApplication<VideoApplication>().getExternalFilesDir(null)?.absolutePath)

    fun extractArguments(arguments: Bundle?) {
        videoPath.value = arguments?.getString(VideoCropFragment.videoPathKey)
    }

    private fun getFileName(): String = File(videoPath.value!!).name

    private fun createOutputFile(): String {
        val fileName = "trim_${getFileName()}"
        return File(rootFile(), fileName).absolutePath

    }

    fun trimVideo(startMs: Int, endMs: Int) {
        val outPath = createOutputFile()
        val observable = repo.trimVideo(
            videoPath.value!!,
            startMs,
            endMs, outPath
        ).subscribeOn(Schedulers.io())
            .doOnSubscribe {
                showLoadingIndicator.postValue(true)
            }
            .doOnNext {
                onNextTrimVideo(it)
            }
            .doOnError {
                errMsg.postValue(it.message)
                showLoadingIndicator.postValue(false)
            }.doOnComplete {
                showLoadingIndicator.postValue(false)
                renameFile()
            }

        compositeDisposable.add(
            observable.subscribe(
                { Log.v(tag, "compressing") },
                { Log.v(tag, it.message.toString()) }
            )
        )
    }

    private fun onNextTrimVideo(onNextValue: Any) {
        if (onNextValue is Boolean) {
            if (onNextValue) {
                back.postValue(true)
            }
        } else {
            showLoadingIndicator.postValue(true)
        }
    }

    private fun renameFile() {
        val dir = File(videoPath.value!!)
        if (dir.exists()) {
            val to = File(videoPath.value)
            val from = File(createOutputFile())
            if (from.exists()) from.renameTo(to)
        }
    }

    fun deleteVideoFile() {
        val dir = File(videoPath.value!!)
        if (dir.exists()) {
            dir.delete()
        }
    }
}