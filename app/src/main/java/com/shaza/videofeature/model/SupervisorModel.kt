package com.shaza.videofeature.homemodule.model

import com.google.firebase.firestore.PropertyName
import com.shaza.videofeature.model.HealthWorkerModel
import com.shaza.videofeature.signupmodule.model.UserModel

/**
 * Created by Shaza Hassan on 23-Nov-2021
 */
data class SupervisorModel(
    @get:PropertyName("health_worker")
    @PropertyName("health_worker")
    val healthWorkers: List<HealthWorkerModel>
) : UserModel() {
    constructor() : this(listOf())
}
