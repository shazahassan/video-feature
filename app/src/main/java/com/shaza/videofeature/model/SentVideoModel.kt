package com.shaza.videofeature.model

import com.google.firebase.firestore.PropertyName
import com.shaza.videofeature.helper.datehelper.GetCurrentDate

data class SentVideoModel(
    @get:PropertyName("video_name")
    @PropertyName("video_name")
    var videoName: String,
    var duration: String,
    @get:PropertyName("sent_time")
    @PropertyName("sent_time")
    var sentTime: String,
    var seen: Boolean
) {
    constructor() : this("", "00:00", GetCurrentDate.getCurrentDate(), false)
}
