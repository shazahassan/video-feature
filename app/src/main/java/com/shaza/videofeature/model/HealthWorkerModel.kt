package com.shaza.videofeature.model

import com.google.firebase.firestore.PropertyName
import com.shaza.videofeature.signupmodule.model.UserModel
import java.io.Serializable

/**
 * Created by Shaza Hassan on 01-Dec-2021
 */

data class HealthWorkerModel(
    @get:PropertyName("supervisor_id")
    @PropertyName("supervisor_id")
    val supervisorId: String = ""
) : Serializable, UserModel() {
    constructor() : this("")
}