package com.shaza.videofeature.model

import android.net.Uri
import com.shaza.videofeature.helper.datehelper.GetCurrentDate
import com.shaza.videofeature.homemodule.model.ProgressModel

/**
 * Created by Shaza Hassan on 27-Nov-2021
 */
data class VideoModel(
    var fileName: String? = null,
    var filePath: String? = null,
    var fileUri: Uri? = null,
    var progressModel: ProgressModel? = null,
    var downloadUrl: String? = null,
    var executeId: Long? = null,
    var sentTime: String = GetCurrentDate.getCurrentDate(),
    var duration: String? = null,
)
