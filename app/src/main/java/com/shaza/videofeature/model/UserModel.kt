package com.shaza.videofeature.signupmodule.model

import com.google.firebase.firestore.PropertyName

/**
 * Created by Shaza Hassan on 18-Nov-2021
 */

open class UserModel() {
    var name: String = ""
    var email: String = ""
    var role: String = ""

    @get:PropertyName("user_id")
    @PropertyName("user_id")
    var userId: String = ""

    constructor(name: String, email: String, role: String, userId: String) : this() {
        this.name = name
        this.email = email
        this.role = role
        this.userId = userId
    }

    constructor(name: String, email: String, role: String) : this() {
        this.name = name
        this.email = email
        this.role = role
    }
}

