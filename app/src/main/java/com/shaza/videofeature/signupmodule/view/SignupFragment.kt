package com.shaza.videofeature.signupmodule.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.textfield.TextInputLayout
import com.shaza.videofeature.MainActivity
import com.shaza.videofeature.R
import com.shaza.videofeature.homemodule.view.HomeFragment
import com.shaza.videofeature.sharedmodule.ErrorPopup
import com.shaza.videofeature.signupmodule.viewmodel.SignupViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.loading_view.*
import kotlinx.android.synthetic.main.signup_fragment.*
import kotlinx.android.synthetic.main.toolbar.*

@AndroidEntryPoint
class SignupFragment : Fragment() {

    companion object {
        fun newInstance() = SignupFragment()
    }

    private val viewModel: SignupViewModel by viewModels()
    private lateinit var roleList: Array<String>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.signup_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initRoleDropdown()
        initObservers()
        initClickListener()
    }

    private fun initToolbar() {
        toolbar.title = getString(R.string.create_new_user)
        toolbar.navigationIcon = requireContext().getDrawable(R.drawable.abc_vector_test)
        toolbar.setNavigationOnClickListener {
            (requireActivity() as MainActivity).onBackPressed()
        }
    }

    private fun initRoleDropdown() {
        roleList = requireContext().resources.getStringArray(R.array.user_roles)
        val dataAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            roleList
        )
        roleAutoCompleteTextview.setAdapter(dataAdapter)
    }

    private fun initObservers() {
        handleShowError()

        handleShowLoading()

        handleCreateUser()
    }

    private fun handleShowLoading() {
        viewModel.showLoading.observe(viewLifecycleOwner, {
            loadingIndicator(it)
        })
    }

    private fun handleShowError() {
        viewModel.errorMsg.observe(viewLifecycleOwner, {
            errorPopup(it)
        })
    }

    private fun handleCreateUser() {
        viewModel.userUid.observe(viewLifecycleOwner, {
            val activity = (requireActivity() as MainActivity)
            activity.removePreviousFragment()
            activity.goToFragment(HomeFragment.newInstance(it))
        })
    }

    private fun initClickListener() {
        nameTextInputEditText.doOnTextChanged { text, _, _, _ ->
            removeFieldErrors(nameTextInputLayout)
            viewModel.updateName(text.toString())
        }

        emailTextInputEditText.doOnTextChanged { text, _, _, _ ->
            removeFieldErrors(emailTextInputLayout)
            viewModel.updateEmail(text.toString())
        }

        passwordTextInputEditText.doOnTextChanged { text, _, _, _ ->
            removeFieldErrors(passwordTextInputLayout)
            viewModel.updatePassword(text.toString())
        }

        roleAutoCompleteTextview.setOnItemClickListener { adapterView, view, i, l ->
            removeFieldErrors(roleTextInputLayout)
            if (this::roleList.isInitialized) {
                viewModel.updateRole(roleList[i])
            }
        }

        createUser.setOnClickListener {
            it.isEnabled = false
            checkFields()
            createUser()
            it.isEnabled = true
        }
    }

    private fun checkFields() {
        if (viewModel.nameIstEmptyOrNull()) {
            showFieldErrors(nameTextInputLayout, getString(R.string.name_error))
        }

        if (viewModel.emailIsEmptyOrNull()) {
            showFieldErrors(emailTextInputLayout, getString(R.string.email_error))
        }

        if (viewModel.passwordIsEmptyOrNull()) {
            showFieldErrors(passwordTextInputLayout, getString(R.string.password_error))
        }

        if (viewModel.roleIsEmptyOrNull()) {
            showFieldErrors(roleTextInputLayout, getString(R.string.role_error))
        }
    }

    private fun showFieldErrors(textInputLayout: TextInputLayout, errorText: String) {
        viewModel.errorInForm = true
        textInputLayout.isErrorEnabled = true
        textInputLayout.error = errorText
    }

    private fun removeFieldErrors(textInputLayout: TextInputLayout) {
        viewModel.errorInForm = false
        textInputLayout.isErrorEnabled = false
        textInputLayout.error = null
    }

    private fun createUser() {
        if (!viewModel.errorInForm) {
            viewModel.createUser()
        }
    }

    private fun errorPopup(errorMsg: String) {
        ErrorPopup.show(requireContext(), errorMsg)
    }

    private fun loadingIndicator(showLoading: Boolean) {
        if (showLoading) {
            progress.visibility = VISIBLE
        } else {
            progress.visibility = GONE
        }
    }
}