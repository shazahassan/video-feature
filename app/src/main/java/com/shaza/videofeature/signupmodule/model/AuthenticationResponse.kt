package com.shaza.videofeature.signupmodule.model

/**
 * Created by Shaza Hassan on 18-Nov-2021
 */
data class AuthenticationResponse(
    val userUid: String,
    val userCreated: Boolean
)
