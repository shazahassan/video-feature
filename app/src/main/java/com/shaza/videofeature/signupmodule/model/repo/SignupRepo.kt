package com.shaza.videofeature.signupmodule.model.repo

import com.google.android.gms.tasks.Task
import com.shaza.videofeature.helper.firebaseauthenticationhelper.FirebaseAuthenticationHelper
import com.shaza.videofeature.helper.firebasefirestorehelper.FirebaseFirestoreHelper
import com.shaza.videofeature.signupmodule.model.AuthenticationResponse
import com.shaza.videofeature.signupmodule.model.UserModel
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

/**
 * Created by Shaza Hassan on 18-Nov-2021
 */
class SignupRepo @Inject constructor() {

    fun createUserWithEmailAndPassword(
        email: String,
        password: String
    ): PublishSubject<AuthenticationResponse> {
        val publishSubject: PublishSubject<AuthenticationResponse> = PublishSubject.create()
        FirebaseAuthenticationHelper.signupUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { result ->
                if (result.isSuccessful) {
                    val userUid = FirebaseAuthenticationHelper.currentUser()?.uid
                    val authenticationResponse = userUid?.let { AuthenticationResponse(it, true) }
                    if (authenticationResponse != null) {
                        publishSubject.onNext(authenticationResponse)
                    } else {
                        publishSubject.onError(Throwable("Error while getting current user"))
                    }
                    publishSubject.onComplete()
                } else {
                    result.exception?.let {
                        publishSubject.onError(it)
                        publishSubject.onComplete()
                    }
                }
            }
        return publishSubject
    }

    fun saveUseInDB(user: UserModel, userUid: String): PublishSubject<Boolean> {
        val publishSubject: PublishSubject<Boolean> = PublishSubject.create()
        user.userId = userUid
        FirebaseFirestoreHelper.saveUserInDB(user, userUid)
            .addOnCompleteListener {
                onComplete(publishSubject, it)
            }
        return publishSubject
    }

    private fun onComplete(publishSubject: PublishSubject<Boolean>, task: Task<Void>) {
        if (task.isSuccessful) {
            publishSubject.onNext(true)
            publishSubject.onComplete()
        } else {
            task.exception?.let { publishSubject.onError(it) }
            publishSubject.onComplete()
        }
    }
}