package com.shaza.videofeature.signupmodule.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.shaza.videofeature.R
import com.shaza.videofeature.VideoApplication
import com.shaza.videofeature.helper.internetconnectionhelper.InternetConnectionHelper
import com.shaza.videofeature.signupmodule.model.UserModel
import com.shaza.videofeature.signupmodule.model.repo.SignupRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class SignupViewModel @Inject constructor(
    application: Application,
    private val repo: SignupRepo
) : AndroidViewModel(application) {
    private val tag = this::class.java.simpleName
    private var compositeDisposable = CompositeDisposable()

    private val nameLiveData = MutableLiveData<String>()
    private val emailLiveData = MutableLiveData<String>()
    private val passwordLiveData = MutableLiveData<String>()
    private val roleLiveData = MutableLiveData<String>()
    var errorInForm: Boolean = false

    val showLoading = MutableLiveData<Boolean>()
    val errorMsg = MutableLiveData<String>()

    val userUid = MutableLiveData<String>()

    fun updateName(name: String) {
        nameLiveData.value = name
    }

    fun updateEmail(email: String) {
        emailLiveData.value = email
    }

    fun updatePassword(password: String) {
        passwordLiveData.value = password
    }

    fun updateRole(role: String) {
        roleLiveData.value = role
    }

    fun nameIstEmptyOrNull(): Boolean {
        return nameLiveData.value?.trim()?.isEmpty() ?: true
    }

    fun emailIsEmptyOrNull(): Boolean {
        return emailLiveData.value?.trim()?.isEmpty() ?: true
    }

    fun passwordIsEmptyOrNull(): Boolean {
        return passwordLiveData.value?.trim()?.isEmpty() ?: true
    }

    fun roleIsEmptyOrNull(): Boolean {
        return roleLiveData.value?.trim()?.isEmpty() ?: true
    }

    fun createUser() {
        if (isConnectedWithInternet()) {
            createAuthUser()
        } else {
            errorMsg.value =
                getApplication<VideoApplication>().getString(R.string.internet_connection_error)
        }
    }

    private fun isConnectedWithInternet(): Boolean {
        return InternetConnectionHelper.isConnected(getApplication())
    }

    private fun createAuthUser() {
        val observable =
            repo.createUserWithEmailAndPassword(emailLiveData.value!!, passwordLiveData.value!!)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    showLoading.postValue(true)
                }
                .doOnNext {
                    if (it.userCreated) {
                        saveUserInFirestore(it.userUid)
                    }
                }
                .doOnError {
                    errorMsg.postValue(it.message)
                    showLoading.postValue(false)
                }
                .doOnComplete {
                    showLoading.postValue(false)
                }

        compositeDisposable.add(
            observable.subscribe(
                { Log.d(tag, "authentication succeeded") }, { Log.d(tag, "authentication failed") }
            )
        )
    }

    private fun saveUserInFirestore(userUid: String) {
        val user = UserModel(nameLiveData.value!!, emailLiveData.value!!, roleLiveData.value!!)
        val observable = repo.saveUseInDB(user, userUid)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                showLoading.postValue(true)
            }.doOnNext {
                this.userUid.postValue(userUid)
            }.doOnError {
                errorMsg.postValue(it.message)
            }.doOnComplete {
                showLoading.postValue(false)
            }

        compositeDisposable.add(observable.subscribe(
            { Log.v(tag, "add in DB succeeded") }, { Log.v(tag, "add in DB failed") }
        ))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}