package com.shaza.videofeature

import android.app.Application
import com.shaza.videofeature.helper.sharedpreferencehelper.SharedPreferenceHelper
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

/**
 * Created by Shaza Hassan on 16-Nov-2021
 */

@HiltAndroidApp
class VideoApplication @Inject constructor() : Application() {
    override fun onCreate() {
        super.onCreate()
        SharedPreferenceHelper.initSharedPreferences(this)
    }
}